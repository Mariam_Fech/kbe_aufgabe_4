package de.htwberlin.viewService.inter;


import java.util.List;
import java.util.Map;

import de.htwberlin.viewService.exceptions.NotNumberInputException;
import de.htwberlin.viewService.exceptions.NumberOutOfRangeException;

/**
 * The Interface ViewService.
 */
public interface ViewServiceInt {

	/**
	 * Asks player to give his name, reads input
	 * @return String player name
	 */
	String outputSayYourName();

	/**
	 * Asks to choose one of thw gamemodes, reads input
	 * @return int choice
	 * @throws NotNumberInputException 
	 * @throws NumberOutOfRangeException 
	 */
	int outputChooseGameMode() throws NotNumberInputException, NumberOutOfRangeException;
	
	/**
	 * Displays the size of bot deck
	 * @param index Bot Number
	 * @param Integer botCardsCount
	 */
	void outputBotCards(List<Integer> botCardsCounts);
	
	/**
	 * Displays player cards
	 * @param List<String> playerCards 
	 */
	void outputPlayerCards(List<String> playerCards);
	
	/**
	 * Displays open card on open deck
	 * @param String card open card
	 */
	void outputOpenCard(String card);
	
	/**
	 * Displays player cards and actions then asking to choose, reads input and maps it
	 * @param playerCards
	 * @param String condition condition to show the right actions
	 * @return Map<String, Integer> map for mapping the choice of player
	 * @throws NotNumberInputException 
	 * @throws NumberOutOfRangeException 
	 */
	Map<String, Integer> outputYourMove(List<String> playerCards, String condition) throws NotNumberInputException, NumberOutOfRangeException;

	/**
	 * Displays information that chodes move is invalid
	 */
	void outputInvalidMove();

	/**
	 * Displays that "mau" rule was violated and it was punished
	 */
	void outputPunishMau();

	/**
	 * Displays that player said mau
	 * @param namePlayer name of player who said mau
	 */
	void outputMau(String namePlayer);

	/**
	 * Displays maumau who said maumau 
	 * @param String namePlayer name of winner
	 */
	void outputMauMau(String namePlayer);

	/**
	 * Displays the end scores of the players
	 * @param countScore map with names and scores
	 */
	void outputShowScore(Map<String,Integer> countScore);

	/**
	 * Displays congratulations to the winner
	 * @param namePlayer winner
	 */
	void outputYouWin(String namePlayer);
	
	/**
	 * Displays goodbye
	 */
	void outputGoodBye();

	/**
	 * Asks if user wants to start a new game
	 * @return true if yes
	 */
	boolean newGame();

	/**
	 * Displays the choice of Suits to wish, reads users choice 
	 * @return int number of suit: 1=hearts, 2=diamonds, 3=clubs, 4=spades
	 * @throws NotNumberInputException 
	 * @throws NumberOutOfRangeException 
	 */
	int outputWish() throws NotNumberInputException, NumberOutOfRangeException;

	/**
	 * Displays the wish posted for the next player
	 * @param currentWish
	 */
	void outputCurrentWish(String currentWish);

	/**
 	 * display mau, punishment or winner
	 * @param checkWinner String to select the right statement
	 * @param nichName String to show the Winners name
	 */
	void showMauAndWinner(String checkWinner, String nickName);

	/**
	 * Parses users string input to a number, evaluates, if the number is valid,
	 * if not valid, displayes error message and aska for new input in loop
	 * @param str String input of user
	 * @param checkNumber max. number that can be accepted
	 * @return int choice
	 * @throws NotNumberInputException 
	 * @throws NumberOutOfRangeException 
	 */
	int parseStringToNr(String str, int checkNumber) throws NotNumberInputException, NumberOutOfRangeException;

	/**
	 * Notifies user about an invalid input
	 * expected a number but was not a number
	 * 
	 */
	void userInputNotANumber();

	/**
	 * Notifies user about an invalid input
	 * input number out of expected range
	 */
	void userInputOutOfRange();

	/**
	 * Notifies user about some internal error, game over
	 */
	void someInternalError();


	/**
	 * @param playerNr number of bot whos turn
	 */
	void botTurn(int playerNr);

	/**
	 * asks user to choose the number of bots to play
	 * @return int number of bots wished by user
	 * @throws NotNumberInputException
	 * @throws NumberOutOfRangeException
	 */
	int outputChooseBots() throws NotNumberInputException, NumberOutOfRangeException;

	

	
	
	
}
