package de.htwberlin.viewService.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.springframework.stereotype.Component;

import de.htwberlin.viewService.exceptions.NotNumberInputException;
import de.htwberlin.viewService.exceptions.NumberOutOfRangeException;
import de.htwberlin.viewService.inter.ViewServiceInt;

/**
 * @author HTW Berlin Komponentenbasierte Entwicklung SS2020 Gruppe 5
 *
 */
@Component
public class ViewService implements ViewServiceInt {

	Scanner s = new Scanner(System.in);

	@Override
	public String outputSayYourName() {
		System.out.println("Willkommen zum Spiel MauMau!\n");
		System.out.println("Wie soll ich dich nennen?\n");
		String name = s.nextLine();
		System.out.println("Hallo " + name + "!");
		System.out.println("Wichtige Spielregeln: ");
		System.out.println("wir spielen mit oder ohne  Kontern (im nächsten Schritt kannst du dich entscheiden)");
		System.out.println("Mit Kontern: du kannst eine Aktionskarte 1x mit deiner Karte der gleichen Art schlagen");
		System.out.println("                                und die Auswirkung an den nächsten Spieler weitergeben.");
		System.out.println("Ohne Kontern: Regel der Aktionskarte muss befolgt werden.");
		System.out.println();
		System.out.println("Aktionskarten:");
		System.out.println(
				"SIEBEN  -> der nächste muss 2 Karten nehmen und die Runde auslassen oder 1x mit Sieben kontern");
		System.out.println("ACHT    -> der nächste muss die Runde auslassen oder 1x mit Acht kontern");
		System.out.println("Bube    -> wünsche dir die Farbe, die der nächste Spieler legen muss");
		System.out.println();
		System.out.println("Gewonnen hat, wer als erster alle Karten abgelegt hat: M A U M A U");
		System.out.println();
		System.out.println("ACHTUNG:");
		System.out.println("Wenn bei dir ZWEI KARTEN ÜBRIG geblieben sind und du eine Karte legen möchtest, ");
		System.out.println("MUSST du VOR der Eingabe der Kartennummer den WERT für M A U eingeben,");
		System.out.println("(sonst zählt das nicht ;-D");
		System.out.println();
		System.out.println("Und jetzt viel Spass!");
		return name;
	}

	@Override
	public int outputChooseGameMode() throws NotNumberInputException, NumberOutOfRangeException {
		System.out.println("Wähle eine Spielvariante:  (gib die Zahl ein)");
		System.out.println("[1]: mit 1x Kontern");
		System.out.println("[2]: ohne Kontern");
		String str = s.nextLine();
		return parseStringToNr(str, 2);
	}

	@Override
	public void outputBotCards(List<Integer> botCards) {
		for (int i = 0; i < botCards.size(); i++) {
			System.out.println("Karten von Bot " + (i+1) + " :  ");
			for(int j = 0; j < botCards.get(i); j++) {
				System.out.print(" --- ");
			}
			System.out.println();
			for(int j = 0; j < botCards.get(i); j++) {
				System.out.print(" |X| ");
			}
			System.out.println();
			for(int j = 0; j < botCards.get(i); j++) {
				System.out.print(" --- ");
			}
			System.out.println();
		}
	}

	@Override
	public void outputPlayerCards(List<String> playerCards) {
		System.out.println("\t\tDeine Karten:");
		int i = 1;
		for (String s : playerCards) {
			System.out.println("\t-----------------------");
			System.out.printf("\t|%21S|\t  [%d]%n", s, i);
			System.out.println("\t-----------------------");
			i++;
		}
	}

	@Override
	public void outputOpenCard(String card) {
		System.out.println("\t\t\t\tOffene Karte:");
		System.out.println("\t\t\t-------------------------");
		System.out.println("\t\t\t|                       |");
		System.out.printf("\t\t\t|%23s|%n", card);
		System.out.println("\t\t\t|                       |");
		System.out.println("\t\t\t-------------------------");
		System.out.println();
	}

	@Override
	public void outputCurrentWish(String currentWish) {
		System.out.println("\t\t\t----------------------------------");
		System.out.println("\t\t\t|              Wunsch            |");
		System.out.print("\t\t\t|           ");
		System.out.printf("%10S", currentWish);
		System.out.println("           |");
		System.out.println("\t\t\t----------------------------------");
	}

	@Override
	public void outputPunishMau() {
		System.out.println("\t\t\tM A U sagen, wenn nach dem Zug nur 1 Karte bleibt!   2 Strafkarten!");
	}

	@Override
	public int outputWish() throws NotNumberInputException, NumberOutOfRangeException {
		System.out.println("\t\t\tWelche Farbe wünschst du dir?\n");
		System.out.println("\t\tHearts\t		[1]");
		System.out.println("\t\tDiamonds\t	[2]");
		System.out.println("\t\tClubs\t		[3]");
		System.out.println("\t\tSpades \t		[4]");

		String str = s.nextLine();
		return parseStringToNr(str, 4);
	}

	@Override
	public void outputMau(String namePlayer) {
		System.out.println("\t" + namePlayer + " sagt Mau");
	}

	@Override
	public void outputMauMau(String namePlayer) {
		System.out.println("\t\t\t" + namePlayer + " sagt MauMau!");
		System.out.println();
	}

	@Override
	public void outputYouWin(String namePlayer) {
		System.out.println("\t\t\t" + namePlayer + " hat gewonnen!");
		System.out.println();
	}

	@Override
	public void outputShowScore(Map<String, Integer> countScore) {
		System.out.println("\t\t\tPunktestand:");
		for (Map.Entry<String, Integer> entry : countScore.entrySet()) {
			System.out.println("\t\t\t" + entry.getKey() + " : " + entry.getValue());
			System.out.println();
		}
	}

	@Override
	public boolean newGame() {
		System.out.println("\t\t\tNeues Spiel, neues Glück! Möchtest du nochmal spielen?\n");
		System.out.println("\t\t\tJa   [1]");
		System.out.println();
		System.out.println("\t\t\tNein [beliebige andere Taste]");
		System.out.println();
		String in = s.nextLine();
		if (in.equals("1"))
			return true;
		else
			return false;
	}

	@Override
	public void outputGoodBye() {
		System.out.println("\t\t\tTschüss! Bis zum nächsten Mal!");

	}
	
	/**
	 * Reads user input as move choice, if first input is mau, 
	 * then a second input is expected and mau is mapped
	 * @param cardsCount amount of users cards
	 * @param map HashMap to map users choice
	 * @param action special action to be mapped if chosen
	 * @throws NotNumberInputException 
	 * @throws NumberOutOfRangeException 
	 */
	private void inputWithMauOption(int cardsCount, Map<String, Integer> map, String action) throws NotNumberInputException, NumberOutOfRangeException {
		boolean valid=false;
		while(valid==false) {
			String string1 = s.nextLine();
			int input1 = parseStringToNr(string1, cardsCount + 2);
			if (input1 == cardsCount + 2) {
				map.put("mau", 1);
				String string2 = s.nextLine();
				int input2 = parseStringToNr(string2, cardsCount + 1);
				if (input2 == cardsCount + 1) {
					map.put(action, 1);
					valid = true;
				} 
				if(input2 > 0 && input2 <= cardsCount){
					map.put("card", input2);
					valid = true;
				}
			}
			else {
				if (input1 == cardsCount + 1) {
					map.put(action, 1);
					valid = true;
				} 
				if(input1 > 0 && input1 <= cardsCount) {
					map.put("card", input1);
					valid = true;
				}
			}
		}
	}
	
	@Override
	public int parseStringToNr(String str, int checkNumber) throws NotNumberInputException, NumberOutOfRangeException {
		int input = 0;
		try {
			input = Integer.parseInt(str);
		} catch (NumberFormatException e) {
			throw new NotNumberInputException("Number input expected but was "+e.getMessage(),e.getCause());
		}
		if (input <= 0 || input > checkNumber)
			throw new NumberOutOfRangeException("Expected Number between 0 and"+checkNumber+" but was "+input);
		return input;
	}

	@Override
	public Map<String, Integer> outputYourMove(List<String> playerCards, String condition) throws NotNumberInputException, NumberOutOfRangeException {
		Map<String, Integer> map = new HashMap<String, Integer>();
		int cardsCount = playerCards.size();
		switch (condition) {
		case "NO_EFFECT":
			showSimpleAction(cardsCount, map);
			break;
		case "PLUS_TWO":
			showTakeTwo(cardsCount, map);
			break;
		case "PLUS_TWO_STRICT":
			showTakeTwoStrict(cardsCount, map);
			break;
		case "PLUS_FOUR":
			showTakeFour(cardsCount, map);
			break;
		case "SKIP":
			showSkip(cardsCount, map);
			break;
		case "SKIP_SKIP":
			showSkipSkip(cardsCount, map);
			break;
		case "WISH":
			showWish(cardsCount, map);
			break;
		case "WISH_WISH":
			showWishWish(cardsCount, map);
			break;
		}
		return map;
	}
	
	/**
	 * Defines output format of action choice when condition is wish wish
	 * @param cardsCount users cards count
	 * @param map Hashmap the choice to map to 
	 * @throws NotNumberInputException 
	 * @throws NumberOutOfRangeException 
	 */
	private void showWishWish(int cardsCount, Map<String, Integer> map) throws NotNumberInputException, NumberOutOfRangeException {
		System.out.println("\t-----------------------");
		System.out.println("\t    1 Karte nehmen        [" + (cardsCount + 1) + "]");
		System.out.println("\t-----------------------");
		System.out.println("\t-----------------------");
		System.out.println("\t         M A U            [" + (cardsCount + 2) + "]");
		System.out.println("\t-----------------------");
		System.out.println("Du bist am Zug, du kannst mit Bube wünschen:");
		inputWithMauOption(cardsCount, map, "take_one");

	}

	/**
	 * Defines output format of action choice when condition is wish
	 * @param cardsCount users cards count
	 * @param map Hashmap the choice to map to 
	 * @throws NotNumberInputException 
	 * @throws NumberOutOfRangeException 
	 */
	private void showWish(int cardsCount, Map<String, Integer> map) throws NotNumberInputException, NumberOutOfRangeException {
		System.out.println("\t-----------------------");
		System.out.println("\t    1 Karte nehmen        [" + (cardsCount + 1) + "]");
		System.out.println("\t-----------------------");
		System.out.println("\t-----------------------");
		System.out.println("\t         M A U            [" + (cardsCount + 2) + "]");
		System.out.println("\t-----------------------");
		System.out.println("Du bist am Zug, du kannst mit Bube wünschen :");
		inputWithMauOption(cardsCount, map, "take_one");
	}

	/**
	 * Defines output format of action choice when condition is skip skip
	 * @param cardsCount users cards count
	 * @param map Hashmap the choice to map to 
	 */
	private void showSkipSkip(int cardsCount, Map<String, Integer> map) {
		System.out.println("\t\tDu musst diese Runde auslassen.");
		map.put("skip_skip", 1);
	}

	/**
	 * Defines output format of action choice when condition is skip
	 * @param cardsCount users cards count
	 * @param map Hashmap the choice to map to 
	 * @throws NotNumberInputException 
	 * @throws NumberOutOfRangeException 
	 */
	private void showSkip(int cardsCount, Map<String, Integer> map) throws NotNumberInputException, NumberOutOfRangeException {
		System.out.println("\t-----------------------");
		System.out.println("\t        Passen            [" + (cardsCount + 1) + "]");
		System.out.println("\t-----------------------");
		System.out.println("\t-----------------------");
		System.out.println("\t        M A U             [" + (cardsCount + 2) + "]");
		System.out.println("\t-----------------------");
		System.out.println("Du bist am Zug, du kannst mit einer Acht kontern:");
		inputWithMauOption(cardsCount, map, "skip");
	}

	/**
	 * Defines output format of action choice when condition is plus 4
	 * @param cardsCount users cards count
	 * @param map Hashmap the choice to map to 
	 */
	private void showTakeFour(int cardsCount, Map<String, Integer> map) {
		System.out.println("\t\tDu musst 4 Karten nehmen.");
		map.put("take_four", 1);
	}

	/**
	 * Defines output format of action choice when condition is plus 2
	 * @param cardsCount users cards count
	 * @param map Hashmap the choice to map to 
	 * @throws NotNumberInputException 
	 * @throws NumberOutOfRangeException 
	 */
	private void showTakeTwo(int cardsCount, Map<String, Integer> map) throws NotNumberInputException, NumberOutOfRangeException {
		System.out.println("\t-----------------------");
		System.out.println("\t    2 Karten nehmen       [" + (cardsCount + 1) + "]");
		System.out.println("\t-----------------------");
		System.out.println("\t-----------------------");
		System.out.println("\t       M A U              [" + (cardsCount + 2) + "]");
		System.out.println("\t-----------------------");
		System.out.println("Du bist am Zug, du kannst mit einer Sieben kontern:");
		inputWithMauOption(cardsCount, map, "take_two");
	}

	/**
	 * Defines output format of action choice when condition is plus 2 strict
	 * @param cardsCount users cards count
	 * @param map Hashmap the choice to map to 
	 */
	private void showTakeTwoStrict(int cardsCount, Map<String, Integer> map) {
		System.out.println("\t\tDu musst 2 Karten nehmen.");
		map.put("take_two", 1);
	}

	/**
	 * Defines output format of action choice when condition is no effect
	 * @param cardsCount users cards count
	 * @param map Hashmap the choice to map to 
	 * @throws NotNumberInputException 
	 * @throws NumberOutOfRangeException 
	 */
	private void showSimpleAction(int cardsCount, Map<String, Integer> map) throws NotNumberInputException, NumberOutOfRangeException {
		System.out.println("\t-----------------------");
		System.out.println("\t    1 Karte nehmen         [" + (cardsCount + 1) + "]");
		System.out.println("\t-----------------------");
		System.out.println("\t-----------------------");
		System.out.println("\t       M A U               [" + (cardsCount + 2) + "]");
		System.out.println("\t-----------------------");
		System.out.println("Du bist am Zug:");
		inputWithMauOption(cardsCount, map, "take_one");
	}
	
	@Override
	public void showMauAndWinner(String checkWinner, String nickName) {
		switch (checkWinner) {
		case "proceed":
			break;
		case "nomau":
			outputPunishMau();
			break;
		case "evalmau":
			outputPunishMau();
			break;
		case "mau":
			outputMau(nickName);
			break;
		case "winner":
			outputMauMau(nickName);
			break;
		}
	}
	
	@Override
	public void outputInvalidMove() {
		System.out.println("\t\t\tDieser Zug ist nicht erlaubt.");
	}

	@Override
	public void userInputNotANumber() {
		System.out.println("Bitte eine Zahl eingeben!");
		
	}

	@Override
	public void userInputOutOfRange() {
		System.out.println("Bitte eine gültige Zahl eingeben!");
		
	}
	
	@Override
	public void someInternalError() {
		System.out.println("Uups, es ist ein Fehler aufgetreten. Versuche es später noch ein Mal.");
		
	}
	
	@Override
	public void botTurn(int playerNr) {
		System.out.println("Bot "+playerNr+" ist dran.......");
		System.out.println();
		System.out.println();
		
	}

	@Override
	public int outputChooseBots() throws NotNumberInputException, NumberOutOfRangeException {
		System.out.println("Gegen wie viele Bots willst du antreten?");
		System.out.println();
		System.out.println("[1]: gegen 1");
		System.out.println("[2]: gegen 2");
		System.out.println("[3]: gegen 3");
		String str = s.nextLine();
		return parseStringToNr(str, 3);
	}
	
}
