package de.htwberlin.viewService.exceptions;

public class NotNumberInputException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public NotNumberInputException(String message, Throwable cause) {
		super(message,cause);
	}
	
}
