package de.htwberlin.viewService.exceptions;

public class NumberOutOfRangeException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public NumberOutOfRangeException(String message) {
		super(message);
	}
	

}
