package de.htwberlin.maumauapp;

import java.lang.System;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.Scanner;

/*
* @author lartsch
*/

public class SelfStarterMain {
	// get the platform string
	public static String systemName = System.getProperty("os.name").toLowerCase();
	
	private static Class<?> cls = null;

	public static void main(String[] args) throws Exception {
		
		cls = Class.forName("de.htwberlin.maumauapp.App");
		
		// check if an argument was passed on jar execution
		if (args.length == 0) {
			// get the path of the currently running jar
			final String jarPath = SelfStarterMain.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			final String decodedPath = URLDecoder.decode(jarPath, "UTF-8");
			// Setting for the terminal window title (Linux/Windows)
			final String windowTitle = "MauMau-Game";
			
			// Check the current platform...
			if (systemName.contains("windows")) {
				// then start the new process with the OS or terminal dependent commands
				new ProcessBuilder(new String[] { "cmd", "/k", "start", "\"" + windowTitle + "\"", "java", "-jar",
						decodedPath.substring(1), "run" }).start();
			} 
			else if (systemName.contains("mac")) {
				new ProcessBuilder(new String[] { "/bin/bash", "-c", "java", "-jar", decodedPath, "run" }).start();
			} 
			else if (systemName.contains("linux")) {
				// TODO: add support for other Linux terminals
				new ProcessBuilder(new String[] { "xfce4-terminal", "--title=" + windowTitle, "--hold", "-x", "java",
						"-jar", decodedPath, "run" }).start();
			} 
			else {
				// No OS could be detected
				System.err.println("OS could not be detected.");
			}
			// destroy the original process
			System.exit(0);
		} else {
			// ACTUAL PROGRAM TO EXECUTE COMES HERE
			Method meth = cls.getMethod("main", String[].class);
			String[] params = null;
			meth.invoke(null, (Object)params);
			
			waitForEnter();
		}
	}
	// https://stackoverflow.com/questions/6032118/make-the-console-wait-for-a-user-input-to-close
	private static void waitForEnter() {
		try(Scanner s = new Scanner(System.in)){
			System.out.println("Press enter to continue ...");
			s.nextLine();
		}
		
	}
}
