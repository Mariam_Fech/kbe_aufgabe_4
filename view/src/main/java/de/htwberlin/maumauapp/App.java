package de.htwberlin.maumauapp;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;

import de.htwberlin.gameService.domain.Game;
import de.htwberlin.gameService.domain.Player;
import de.htwberlin.gameService.exceptions.CardNotPlacedException;
import de.htwberlin.gameService.exceptions.InvalidMoveException;
import de.htwberlin.gameService.impl.GameService;
import de.htwberlin.viewService.exceptions.NotNumberInputException;
import de.htwberlin.viewService.exceptions.NumberOutOfRangeException;
import de.htwberlin.viewService.impl.ViewService;

/**
 * Application Class controlling the dislay and flow of MauMau Game
 */
@Controller
public class App {

	
	private ViewService viewService;
	@Autowired
	public void setViewService(ViewService viewService) {
		this.viewService = viewService;
	}
	private GameService gameService;
	@Autowired
	public void setGameService(GameService gameService) {
		this.gameService = gameService;
	}

	private static final Logger LOGGER = LogManager.getLogger(App.class);

	private static boolean newGame = false;

	private static ConfigurableApplicationContext container = new AnnotationConfigApplicationContext("de.htwberlin");

	public static void main(String[] args) {
		App app = container.getBean(App.class);
		LOGGER.log(Level.INFO, "MauMauApp gestartet");
		while (app.run());
		LOGGER.log(Level.INFO, "MauMauApp beendet");
	}

	/**
	 * Starts, controls and terminates maumau game
	 * 
	 * @return true if user wants a new game
	 */
	public boolean run() {
		EntityManagerFactory emf = null;
		EntityManager em = null;
		try {
			emf = Persistence.createEntityManagerFactory("maumau");
	    	em = emf.createEntityManager();
	    	LOGGER.log(Level.INFO, "EntityManager set up successfully");
		}catch(Exception e) {
			viewService.someInternalError();
			LOGGER.log(Level.ERROR, "Could not connect to Database\n" + e.getStackTrace()+e.getCause()+e.getMessage());
			return false;
		}
		

		Game game = new Game();
		LOGGER.log(Level.INFO, "MauMauApp gestartet");
		String playerName = viewService.outputSayYourName();
		int gameMode = 0;
		while (gameMode == 0) {
			try {
				gameMode = viewService.outputChooseGameMode();
			} catch (NotNumberInputException e) {
				viewService.userInputNotANumber();
			} catch (NumberOutOfRangeException e) {
				viewService.userInputOutOfRange();
			}
		}
		int bots = 0;
		while (bots == 0) {
			try {
				bots = viewService.outputChooseBots();
			} catch (NotNumberInputException e) {
				viewService.userInputNotANumber();
			} catch (NumberOutOfRangeException e) {
				viewService.userInputOutOfRange();
			}
		}
		gameService.initGame(game, playerName, gameMode, bots);

		while (game.getWinner() == null) {
			if(em!=null) persistGame(game, em);

			boolean validMove = false;
			int playerNr = game.getWhosTurn();
			Player player = game.getPlayers().get(playerNr);
			String condition = game.getCondition().toString();

			if (game.getCurrentWish() != null) {
				LOGGER.log(Level.INFO, "App: current wish " + game.getCurrentWish());
				viewService.outputCurrentWish(game.getCurrentWish());
			}
			while (validMove == false) {
				if (player.getNickname() == playerName) {
					LOGGER.log(Level.INFO, "App: player's turn");
					viewService.outputBotCards(gameService.countBotCards(game));

					Map<String, Integer> playerChoice = null;
					while (playerChoice == null) {
						try {
							viewService.outputOpenCard(gameService.getOpenCard(game).toString());
							List<String> playerCards = gameService.getPlayerCards(game);
							viewService.outputPlayerCards(gameService.getPlayerCards(game));
							playerChoice = viewService.outputYourMove(playerCards, condition);
						} catch (NotNumberInputException e) {
							LOGGER.log(Level.INFO, e.getMessage());
							viewService.userInputNotANumber();
						} catch (NumberOutOfRangeException e) {
							LOGGER.log(Level.INFO, e.getMessage());
							viewService.userInputOutOfRange();
						}
					}
					try {
						validMove = gameService.playerMove(game, playerChoice);
					} catch (InvalidMoveException e) {
						LOGGER.log(Level.INFO, e.getMessage());
						viewService.outputInvalidMove();
						playerChoice.clear();
					} catch (CardNotPlacedException e) {
						LOGGER.log(Level.ERROR, "Player: " + e.getMessage());
						viewService.someInternalError();
						return false;
					}
					if (validMove == true) {
						LOGGER.log(Level.INFO, "App: player choice " + playerChoice.toString());
						viewService.outputOpenCard(gameService.getOpenCard(game).toString());
						validMove = true;
					}
				} else {
					LOGGER.log(Level.INFO, "App: bot's turn");
					viewService.botTurn(playerNr);
					wait(1000);
					try {
						gameService.botMove(game);
					} catch (InvalidMoveException | CardNotPlacedException e) {
						LOGGER.log(Level.ERROR, "Bot " + game.getWhosTurn() + ": " + e.getMessage());
						viewService.someInternalError();
						return false;
					}
					viewService.outputOpenCard(gameService.getOpenCard(game).toString());
					validMove = true;
				}
			}
			if (player.isWish()) {
				boolean lastCardPlayed = gameService.checkWishAndWinner(game, player);
				
				if (lastCardPlayed == false) {			
					if (player.getNickname() == playerName) {
						int currentWish = 0;
						while (currentWish == 0) {
							try {
								currentWish = viewService.outputWish();
								gameService.setCurrentWishPlayer(game, currentWish);
							} catch (NotNumberInputException e) {
								LOGGER.log(Level.INFO, "Player: " + e.getMessage());
								viewService.userInputNotANumber();
							} catch (NumberOutOfRangeException e) {
								LOGGER.log(Level.INFO, "Player: " + e.getMessage());
								viewService.userInputOutOfRange();
							}
						}
					} else {
						gameService.setCurrentWishBot(game, player);
					}
				}
			}
			String checkWinner = gameService.checkCardsAndMau(game, player);
			viewService.showMauAndWinner(checkWinner, player.getNickname());
			gameService.unsetMau(game);
			if (game.getWinner() == null) {
				gameService.nextPlayer(game);
			}
		}
		viewService.outputYouWin(game.getWinner().getNickname());
		viewService.outputShowScore(gameService.countScoreGame(game));

		if(em!=null) persistGame(game, em);
		
		newGame = viewService.newGame();
		if (newGame == false) {
			LOGGER.log(Level.INFO, "App: game over");
			viewService.outputGoodBye();
		}
		if(em!=null) {
			em.close();
	    	emf.close();
		}
		
    	LOGGER.log(Level.INFO, "EntityManager disposed");

		LOGGER.log(Level.INFO, "App: new game");
		return newGame;
	}

	private void persistGame(Game game, EntityManager em) {
		try {
			em.getTransaction().begin();
	    	em.persist(game);
	    	em.getTransaction().commit();
	    	LOGGER.log(Level.INFO, "JPA: game saved");
		}catch(Exception e) {
			em.getTransaction().rollback();
			LOGGER.log(Level.ERROR, "JPA: could not persist game\n" + e.getMessage());
		}
	}

	/**
	 * Pausiert den Spielfluss um angegebene Millisekunden
	 * @param ms
	 */
	public void wait(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}
}
