package de.htwberlin.ruleService.impl;

import java.util.List;
import java.util.Map;


import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import de.htwberlin.deckService.domain.Card;
import de.htwberlin.deckService.domain.Rank;
import de.htwberlin.ruleService.domain.CardEffect;
import de.htwberlin.ruleService.domain.Condition;
import de.htwberlin.ruleService.inter.RuleServiceInt;


@Service
public class RuleService implements RuleServiceInt {

	
	private static final Logger LOGGER = LogManager.getLogger(RuleService.class);

	@Override
	public boolean doubleDeck(int countPlayer) {
		if (countPlayer > 3)
			return true;
		else
			return false;
	}

	/**
	 * Returns the Effect of a Card
	 * seven=plus2, eight=skip, jack=wish, else=no effect
	 * @param currentCard card to be checked
	 * @return CardEffect the effect as enum value
	 */
	private CardEffect getCardEffect(Card currentCard) {
		if (currentCard.getRank() == Rank.SEVEN) {
			LOGGER.log(Level.INFO, "getCardEffect: plus2");
			return CardEffect.PLUS_TWO;
		}
		if (currentCard.getRank() == Rank.EIGHT) {
			LOGGER.log(Level.INFO, "getCardEffect: skip");
			return CardEffect.SKIP;
		}
		if (currentCard.getRank() == Rank.Jack) {
			LOGGER.log(Level.INFO, "getCardEffect: wish");
			return CardEffect.WISH;
		} else {
			LOGGER.log(Level.INFO, "getCardEffect: no effect");
			return CardEffect.NO_EFFECT;
		}
	}

	@Override
	public Condition validateAction(Condition condition, int gameMode, String currentWish,
			Map<String, Integer> playerChoice) {
		return actionAllowed(condition, gameMode, currentWish,playerChoice);
	}

	/**
	 * Checks if action move is allowed
	 * if move is allowed then returned no effect/wishwish, else returned invalid move
	 * @param currentWish 
	 * @param gameMode 
	 * @param condition current condition for move evaluation
	 * @param playerChoice Map<String,Integer> with the mapped move
	 * @return Condition result of the check 
	 */
	private Condition actionAllowed(Condition condition, int gameMode, String currentWish, Map<String, Integer> playerChoice) {
		if (condition == Condition.NO_EFFECT) {
			if (playerChoice.containsKey("take_one")) {
				LOGGER.log(Level.INFO, "actionAllowed: no effect to no effect");
				return Condition.NO_EFFECT;
			}
		}
		if (condition == Condition.PLUS_TWO || condition == Condition.PLUS_TWO_STRICT) {
			if (playerChoice.containsKey("take_two")) {
				LOGGER.log(Level.INFO, "actionAllowed: plus2/strict to no effect");
				return Condition.NO_EFFECT;
			}
		}
		if (condition == Condition.PLUS_FOUR) {
			if (playerChoice.containsKey("take_four")) {
				LOGGER.log(Level.INFO, "actionAllowed: plus4 to no effect");
				return Condition.NO_EFFECT;
			}
		}
		if (condition == Condition.SKIP) {
			if (playerChoice.containsKey("skip")) {
				LOGGER.log(Level.INFO, "actionAllowed: skip to no efect");
				return Condition.NO_EFFECT;
			}
		}
		if (condition == Condition.SKIP_SKIP) {
			if (playerChoice.containsKey("skip_skip")) {
				LOGGER.log(Level.INFO, "actionAllowed: skip skip to no effect");
				return Condition.NO_EFFECT;
			}
		}
		if (condition == Condition.WISH) {
			if (playerChoice.containsKey("take_one")) {
				LOGGER.log(Level.INFO, "actionAllowed: wish to wish wish");
				return Condition.WISH_WISH;
			}
		}
		if (condition == Condition.WISH_WISH) {
			if (playerChoice.containsKey("take_one")) {
				LOGGER.log(Level.INFO, "actionAllowed: wish wish to wish wish");
				return Condition.WISH_WISH;
			}
		}
		LOGGER.log(Level.INFO, "actionAllowed: Invalid Move");
		return Condition.INVALID_MOVE;

	}

	@Override
	public Condition validateCard(Condition condition, int gameMode, String currentWish, Card openCard, Card card) {
		return cardAllowed(condition,currentWish,gameMode,card,openCard);
	}

	/**
	 * Checks if card move allowed according to gamemode
	 * if move is not allowed returned invalid move
	 * @param gameMode  0=with fight back, 1= strict following the action card
	 * @param currentWish current active wish
	 * @param condition current condition for evaluation if the move is allowed
	 * @param currentCard card to be checked if it is allowed
	 * @param lastPlayedCard open card on open deck
	 * @return Condition result of check
	 */
	private Condition cardAllowed(Condition condition, String currentWish, int gameMode, Card currentCard, Card lastPlayedCard) {
		CardEffect effect = getCardEffect(currentCard);

		if (gameMode == 1) {

			if (condition == Condition.NO_EFFECT) {

				if (effect == CardEffect.PLUS_TWO && (currentCard.getRank() == lastPlayedCard.getRank()
						|| currentCard.getSuit() == lastPlayedCard.getSuit())) {
					return Condition.PLUS_TWO;
				}
				if (effect == CardEffect.SKIP && (currentCard.getRank() == lastPlayedCard.getRank()
						|| currentCard.getSuit() == lastPlayedCard.getSuit())) {
					return Condition.SKIP;
				}
				if (effect == CardEffect.WISH) {
					return Condition.WISH;
				}
				if (effect == CardEffect.NO_EFFECT && (currentCard.getRank() == lastPlayedCard.getRank()
						|| currentCard.getSuit() == lastPlayedCard.getSuit())) {
					return Condition.NO_EFFECT;
				}
			}

			if (condition == Condition.PLUS_TWO && effect == CardEffect.PLUS_TWO) {
				return Condition.PLUS_FOUR;
			}
			if (condition == Condition.PLUS_TWO && effect == CardEffect.WISH) {
				return Condition.WISH;
			}
			if (condition == Condition.SKIP && effect == CardEffect.SKIP) {
				return Condition.SKIP_SKIP;
			}
			if (condition == Condition.SKIP && effect == CardEffect.WISH) {
				return Condition.WISH;
			}
			if (condition == Condition.WISH && effect == CardEffect.WISH) {
				return Condition.WISH_WISH;
			}
			if (condition == Condition.WISH || condition == Condition.WISH_WISH) {
				if (currentCard.getSuit().toString() == currentWish && effect == CardEffect.NO_EFFECT) {
					return Condition.NO_EFFECT;
				}
				if (currentCard.getSuit().toString() == currentWish && effect == CardEffect.PLUS_TWO) {
					return Condition.PLUS_TWO;
				}
				if (currentCard.getSuit().toString() == currentWish && effect == CardEffect.SKIP) {
					return Condition.SKIP;
				}
			}
			if (condition == Condition.WISH_WISH) {
				if (currentCard.getSuit().toString() == currentWish && effect == CardEffect.WISH) {
					return Condition.WISH;
				}
			}
			
			LOGGER.log(Level.INFO, "cardAllowed: nothing of gamemode 0");
		}

		if (gameMode == 2) {
			if (condition == Condition.NO_EFFECT) {
				if (effect == CardEffect.PLUS_TWO && (currentCard.getRank() == lastPlayedCard.getRank()
						|| currentCard.getSuit() == lastPlayedCard.getSuit())) {
					return Condition.PLUS_TWO_STRICT;
				}
				if (effect == CardEffect.SKIP && (currentCard.getRank() == lastPlayedCard.getRank()
						|| currentCard.getSuit() == lastPlayedCard.getSuit())) {
					return Condition.SKIP_SKIP;
				}
				if (effect == CardEffect.WISH ) {
					return Condition.WISH_WISH;
				}
				if (effect == CardEffect.NO_EFFECT && (currentCard.getSuit() == lastPlayedCard.getSuit()
						|| currentCard.getRank() == lastPlayedCard.getRank())) {
					return Condition.NO_EFFECT;
				}
			}
			if (condition == Condition.WISH_WISH) {
				if (currentCard.getSuit().toString() == currentWish && effect == CardEffect.NO_EFFECT) {
					return Condition.NO_EFFECT;
				}
				if (currentCard.getSuit().toString() == currentWish && effect == CardEffect.WISH) {
					return Condition.WISH_WISH;
				}
				if (currentCard.getSuit().toString() == currentWish && effect == CardEffect.PLUS_TWO) {
					return Condition.PLUS_TWO_STRICT;
				}
				if (currentCard.getSuit().toString() == currentWish && effect == CardEffect.SKIP) {
					return Condition.SKIP_SKIP;
				}
			}
			if (condition == Condition.PLUS_TWO_STRICT) {
				return Condition.INVALID_MOVE;
			}
			if (condition == Condition.SKIP_SKIP) {
				return Condition.INVALID_MOVE;
			}
		}
		LOGGER.log(Level.INFO, "cardAllowed: nothing of gamemode 1 : invalid move");
		return Condition.INVALID_MOVE;
	}

	@Override
	public Map<String, Integer> botChoice(Condition condition, List<Card> botCards, Card openCard, String wish) {
		return null;
	}

	
	@Override
	public int howMuchToTake(Condition condBefore, Condition condAfter) {
		if (condBefore == Condition.NO_EFFECT && condAfter == Condition.NO_EFFECT) {
			return 1;
		}
		if ((condBefore == Condition.PLUS_TWO || condBefore == Condition.PLUS_TWO_STRICT)
				&& condAfter == Condition.NO_EFFECT) {
			return 2;
		}
		if (condBefore == Condition.PLUS_FOUR && condAfter == Condition.NO_EFFECT) {
			return 4;
		}
		if (condBefore == Condition.WISH && condAfter == Condition.WISH_WISH) {
			return 1;
		}
		if (condBefore == Condition.WISH_WISH && condAfter == Condition.WISH_WISH) {
			return 1;
		}
		if ((condBefore == Condition.SKIP || condBefore == Condition.SKIP_SKIP)
				&& condAfter == Condition.NO_EFFECT) {
			return 0;
		}
		return 0;
	}
	@Override
	public String checkCardsAndMau(int cards, boolean isMau) { 	
		if (cards == 1 && isMau == false) { 
			return"nomau";
		}
		if (cards > 1 && isMau == true) {
			return "evalmau";
		}
		if (cards == 1 && isMau == true) { 
			return "mau";
		}
		if (cards == 0) {
			return "winner";
		}
		else 
			return "proceed";
	}
}
