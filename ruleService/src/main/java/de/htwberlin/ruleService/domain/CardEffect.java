package de.htwberlin.ruleService.domain;

public enum CardEffect {
	/** The no effect. */
	NO_EFFECT,
	
	/** The skip. */
	SKIP,
	
	/** The wish. */
	WISH,
	
	/** The plus two. */
	PLUS_TWO,	
}
