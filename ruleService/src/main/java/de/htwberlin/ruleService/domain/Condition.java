package de.htwberlin.ruleService.domain;

import de.htwberlin.deckService.domain.Card;

// TODO: Auto-generated Javadoc
/**
 * The Enum Condition.
 */
public enum Condition {

	/** The no effect. */
	NO_EFFECT,
	
	/** The plus two. */
	PLUS_TWO,
	
	PLUS_FOUR,
	
	PLUS_TWO_STRICT,
	
	/** The skip. */
	SKIP,
	
	SKIP_SKIP,
	
	WISH,
	
	WISH_WISH,
	
	INVALID_MOVE;
	
}
