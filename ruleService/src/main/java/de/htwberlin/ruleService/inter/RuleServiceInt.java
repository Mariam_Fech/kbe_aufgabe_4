package de.htwberlin.ruleService.inter;
import java.util.List;
import java.util.Map;

import de.htwberlin.deckService.domain.Card;
import de.htwberlin.ruleService.domain.Condition;



public interface RuleServiceInt {
	
	/**
	 * Returns Decision according to Number of Players if to double the deck.
	 * If more than two players in the game then the play deck consists of two identic decks
	 * @param countPlayer
	 * @return boolean true = double deck, false = simple deck
	 */
	public boolean doubleDeck(int countPlayer);
	
	/**
	 * Validates if card placed by player is a valid move
	 * @param condition
	 * @param gameMode
	 * @param currentWish
	 * @param openCard
	 * @param card
	 * @return Condition as effect of the move or invalid move
	 */
	public Condition validateCard(Condition condition, int gameMode, String currentWish, Card openCard, Card card);

	/**
	 * Validates if action chosen by player is a valid move
	 * @param condition
	 * @param gameMode
	 * @param currentWish
	 * @param playerChoice
	 * @return Condition as effect of the move or invalid move
	 */
	public Condition validateAction(Condition condition, int gameMode, String currentWish, Map<String, Integer> playerChoice);

	/**
	 * Returns bots move choice
	 * @param condition current game condition
	 * @param botCards 
	 * @param openCard
	 * @param wish null or String current wish
	 * @return Map<String,Integer> map with mapped choice
	 */
	public Map<String, Integer> botChoice(Condition condition, List<Card> botCards, Card openCard, String wish);
	
	/**
	 * Returns how much the player has to take according to condition change 
	 * caused by his action
	 * @param condBefore Condition before players move
	 * @param condAfter Condition after players move
	 * @return int amount of cards to take
	 */
	int howMuchToTake(Condition condBefore, Condition condAfter);

	/**
	 * Returns a panishment as string if mau used or not used against the rules 
	 * or no punisment if mau used correctly
	 * @param cards int cards amount of player
	 * @param isMau boolean if mau of player is set true
	 * @return String "proceed" |"nomau" if mau expected but not set |"evalmau" if mau set but not expected
	 * 					"mau" if mau used correctly | winner if players cards amount == 0
	 */
	String checkCardsAndMau(int cards, boolean isMau);
	
}

