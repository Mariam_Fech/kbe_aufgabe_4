package de.htwberlin.ruleService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.htwberlin.deckService.domain.Card;
import de.htwberlin.deckService.domain.Rank;
import de.htwberlin.deckService.domain.Suit;
import de.htwberlin.ruleService.domain.Condition;
import de.htwberlin.ruleService.impl.RuleService;

// TODO: Auto-generated Javadoc
/**
 * The Class RuleServiceTest.
 */
public class RuleServiceTest {
	
	/** The rule service. */
	RuleService ruleService;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		ruleService = new RuleService();		
	}

	/**
	 * 
	 */
	@Test
	public void testDoubleDeck_true() {
		int countPlayer = 4;
		boolean actual= ruleService.doubleDeck(countPlayer);
		
		Assert.assertTrue(actual);
	}	
	/**
	 * 
	 */
	@Test
	public void testDoubleDeck_false() {
		int countPlayer = 2;
		boolean actual= ruleService.doubleDeck(countPlayer);
		
		Assert.assertFalse(actual);
	}
	
	
	
	/**
	 * Test validate card move plus4 invalid
	 */
	@Test
	public void testValidateCard_PLUS_FOUR_INVALID_MOVE() {
		Condition condition = Condition.PLUS_FOUR;
		int gameMode = 0;
		String currentWish = null;
		Card openCard = new Card(Suit.Clubs,Rank.SEVEN);
		Card card = new Card(Suit.Hearts,Rank.ACE);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(actual,Condition.INVALID_MOVE);
	}
	
	/**
	 * Test validate card move plus2 strict invalid
	 */
	@Test
	public void testValidateCard_PLUS_TWO_STRICT_INVALID_MOVE() {
		Condition condition = Condition.PLUS_TWO_STRICT;
		int gameMode = 1;
		String currentWish = null;
		Card openCard = new Card(Suit.Clubs,Rank.SEVEN);
		Card card = new Card(Suit.Hearts,Rank.SEVEN);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(actual,Condition.INVALID_MOVE);
	}
	
	/**
	 * Test validate card move skip skip invalid
	 */
	@Test
	public void testValidateCard_SKIP_SKIP_INVALID_MOVE() {
		Condition condition = Condition.SKIP_SKIP;
		int gameMode = 0;
		String currentWish = null;
		Card openCard = new Card(Suit.Clubs,Rank.EIGHT);
		Card card = new Card(Suit.Hearts,Rank.EIGHT);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(actual,Condition.INVALID_MOVE);
	}
	
	/**
	 * Test validate card move wish wish invalid
	 */
	@Test
	public void testValidateCard_WISH_WISH_INVALID_MOVE() {
		Condition condition = Condition.WISH_WISH;
		int gameMode = 0;
		String currentWish = null;
		Card openCard = new Card(Suit.Clubs,Rank.Jack);
		Card card = new Card(Suit.Hearts,Rank.Jack);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(actual,Condition.INVALID_MOVE);
	}
	
	/**
	 *Test validate card move no effect invalid.
	 */
	@Test
	public void testValidateCard_NO_EFFECT_INVALID_MOVE() {
		Condition condition = Condition.NO_EFFECT;
		int gameMode = 0;
		String currentWish = null;
		Card openCard = new Card(Suit.Clubs,Rank.SEVEN);
		Card card = new Card(Suit.Hearts,Rank.ACE);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(Condition.INVALID_MOVE,actual);
	}
	
	/**
	 * Test validate card move no effect no effect.
	 */
	@Test
	public void testValidateCard_NO_EFFECT_ON_EFFECT() {
		Condition condition = Condition.NO_EFFECT;
		int gameMode = 1;
		String currentWish = null;
		Card openCard = new Card(Suit.Clubs,Rank.SIX);
		Card card = new Card(Suit.Hearts,Rank.SIX);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(actual,Condition.NO_EFFECT);
	}
	
	/**
	 * Test validate card move no effect plus2
	 */
	@Test
	public void testValidateCard_NO_EFFECT_PLUS_TWO() {
		Condition condition = Condition.NO_EFFECT;
		int gameMode = 1;
		String currentWish = null;
		Card openCard = new Card(Suit.Clubs,Rank.SEVEN);
		Card card = new Card(Suit.Hearts,Rank.SEVEN);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(actual,Condition.PLUS_TWO);
	}
	
	/**
	 * Test validate card move no effect plus2 strict gamemode 1
	 */
	@Test
	public void testValidateCard_NO_EFFECT_PLUS_TWO_STRICT() {
		Condition condition = Condition.NO_EFFECT;
		int gameMode = 2;
		String currentWish = null;
		Card openCard = new Card(Suit.Clubs,Rank.SEVEN);
		Card card = new Card(Suit.Hearts,Rank.SEVEN);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(actual,Condition.PLUS_TWO_STRICT);
	}
	
	/**
	 * Test validate card move no effect skip
	 */
	@Test
	public void testValidateCard_NO_EFFECT_SKIP() {
		Condition condition = Condition.NO_EFFECT;
		int gameMode = 1;
		String currentWish = null;
		Card openCard = new Card(Suit.Clubs,Rank.SEVEN);
		Card card = new Card(Suit.Clubs,Rank.EIGHT);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(Condition.SKIP,actual);
	}
	
	/**
	 * Test validate card move no effect skip skip gamemode 1
	 */
	@Test
	public void testValidateCard_NO_EFFECT_SKIP_SKIP() {
		Condition condition = Condition.NO_EFFECT;
		int gameMode = 2;
		String currentWish = null;
		Card openCard = new Card(Suit.Clubs,Rank.SEVEN);
		Card card = new Card(Suit.Clubs,Rank.EIGHT);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(actual,Condition.SKIP_SKIP);
	}
	
	/**
	 * Test validate card move no effect wish
	 */
	@Test
	public void testValidateCard_NO_EFFECT_WISH() {
		Condition condition = Condition.NO_EFFECT;
		int gameMode = 1;
		String currentWish = null;
		Card openCard = new Card(Suit.Clubs,Rank.SEVEN);
		Card card = new Card(Suit.Clubs,Rank.Jack);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(actual,Condition.WISH);
	}
	
	/**
	 * Test validate card move no effect wish wish gamemode 1
	 */
	@Test
	public void testValidateCard_NO_EFFECT_WISH_WISH() {
		Condition condition = Condition.NO_EFFECT;
		int gameMode = 2;
		String currentWish = null;
		Card openCard = new Card(Suit.Clubs,Rank.SEVEN);
		Card card = new Card(Suit.Clubs,Rank.Jack);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(actual,Condition.WISH_WISH);
	}
	
	/**
	 * Test validate card move plus2 plus4
	 */
	@Test
	public void testValidateCard_PLUS_TWO_PLUS_FOUR() {
		Condition condition = Condition.PLUS_TWO;
		int gameMode = 1;
		String currentWish = null;
		Card openCard = new Card(Suit.Clubs,Rank.SEVEN);
		Card card = new Card(Suit.Hearts,Rank.SEVEN);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(actual,Condition.PLUS_FOUR);
	}
	
	/**
	 * Test of card validation, gamemode 0, skip to skip_skip
	 */
	@Test
	public void testValidateCard_SKIP_SKIP_SKIP() {
		Condition condition = Condition.SKIP;
		int gameMode = 1;
		String currentWish = null;
		Card openCard = new Card(Suit.Clubs,Rank.EIGHT);
		Card card = new Card(Suit.Hearts,Rank.EIGHT);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(actual,Condition.SKIP_SKIP);
	}
	
	/**
	 * Test of card validation gamemode 0, wish to wish_wish
	 */
	@Test
	public void testValidateCard_WISH_To_WISH_WISH() {
		Condition condition = Condition.WISH;
		int gameMode = 1;
		String currentWish = "Clubs";
		Card openCard = new Card(Suit.Clubs,Rank.Jack);
		Card card = new Card(Suit.Hearts,Rank.Jack);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(actual,Condition.WISH_WISH);
	}
	
	/**
	 * Test of card validation gamemode 0, wish_wish to wish
	 */
	@Test
	public void testValidateCard_WISH_WISH_To_WISH() {
		Condition condition = Condition.WISH_WISH;
		int gameMode = 1;
		String currentWish = Suit.Hearts.toString();
		Card openCard = new Card(Suit.Clubs,Rank.Jack);
		Card card = new Card(Suit.Hearts,Rank.Jack);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(Condition.WISH, actual);
	}

	/**
	 * Test of card validation gamemode 0, wish wish to no effect
	 */
	@Test
	public void testValidateCard_WISH_WISH_To_NO_EFFECT() {
		Condition condition = Condition.WISH_WISH;
		int gameMode = 1;
		String currentWish = Suit.Hearts.toString();
		Card openCard = new Card(Suit.Clubs,Rank.Jack);
		Card card = new Card(Suit.Hearts,Rank.SIX);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(Condition.NO_EFFECT, actual);
	}
	
	/**
	 * Test of card validation gamemode 0, wish to no effect
	 */
	@Test
	public void testValidateCard_WISH_NO_EFFECT() {
		Condition condition = Condition.WISH;
		int gameMode = 1;
		String currentWish = Suit.Hearts.toString();
		Card openCard = new Card(Suit.Clubs,Rank.Jack);
		Card card = new Card(Suit.Hearts,Rank.SIX);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(Condition.NO_EFFECT, actual);
	}
	
	/**
	 * Test of card validation gamemode 0, wish to plus2
	 */
	@Test
	public void testValidateCard_WISH_WISH_PLUS_TWO() {
		Condition condition = Condition.WISH_WISH;
		int gameMode = 1;
		String currentWish = Suit.Hearts.toString();
		Card openCard = new Card(Suit.Clubs,Rank.Jack);
		Card card = new Card(Suit.Hearts,Rank.SEVEN);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(Condition.PLUS_TWO, actual);
	}
	
	/**
	 * Test of card validation gamemode 1, wish wish to plus2 strict
	 */
	@Test
	public void testValidateCard_WISH_WISH_PLUS_TWO_STRICT() {
		Condition condition = Condition.WISH_WISH;
		int gameMode = 2;
		String currentWish = Suit.Hearts.toString();
		Card openCard = new Card(Suit.Clubs,Rank.Jack);
		Card card = new Card(Suit.Hearts,Rank.SEVEN);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(Condition.PLUS_TWO_STRICT, actual);
	}
	
	/**
	 * Test of card validation gamemode 1, skip skip to skip
	 */
	@Test
	public void testValidateCard_WISH_WISH_SKIP_SKIP() {
		Condition condition = Condition.WISH_WISH;
		int gameMode = 2;
		String currentWish = Suit.Hearts.toString();
		Card openCard = new Card(Suit.Clubs,Rank.Jack);
		Card card = new Card(Suit.Hearts,Rank.EIGHT);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(Condition.SKIP_SKIP, actual);
	}
	
	
	/**
	 * Test of card validation gamemode 0, wish_wish to skip
	 */
	@Test
	public void testValidateCard_WISH_WISH_SKIP() {
		Condition condition = Condition.WISH_WISH;
		int gameMode = 1;
		String currentWish = Suit.Hearts.toString();
		Card openCard = new Card(Suit.Clubs,Rank.Jack);
		Card card = new Card(Suit.Hearts,Rank.EIGHT);
		
		Condition actual = ruleService.validateCard(condition, gameMode, currentWish, openCard, card);
		
		Assert.assertEquals(Condition.SKIP, actual);
	}
	
	
	
	
	/**
	 * Test validate action: gamemode 0, no effect invalid 
	 */
	@Test
	public void testValidateAction_NO_EFFECT_INVALID_MOVE() {
		Condition condition = Condition.NO_EFFECT;
		int gameMode = 1;
		String currentWish = null;
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("take_two", 1); //"take_four","skip","skip_skip"
		
		Condition actual = ruleService.validateAction(condition, gameMode, currentWish, playerChoice);
		
		Assert.assertEquals(Condition.INVALID_MOVE, actual);
	}
	
	/**
	 * Test validate action: gamemode 0,  no efect to no effect take1
	 */
	@Test
	public void testValidateAction_NO_EFFECT_take_one() {
		Condition condition = Condition.NO_EFFECT;
		int gameMode = 1;
		String currentWish = null;
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("take_one", 1);
		
		Condition actual = ruleService.validateAction(condition, gameMode, currentWish,playerChoice);
		
		Assert.assertEquals(Condition.NO_EFFECT, actual);
	}
	
	/**
	 * Test validate action: gamemode 0,  wish to wish wish take1
	 */
	@Test
	public void testValidateAction_WISH_take_one() {
		Condition condition = Condition.WISH ;
		int gameMode = 1;  
		String currentWish = Suit.Clubs.toString();
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("take_one", 1);
		
		Condition actual = ruleService.validateAction(condition, gameMode, currentWish,playerChoice);
		
		Assert.assertEquals(Condition.WISH_WISH, actual);
	}
	
	/**
	 * Test validate action: gamemode 0,  wish wish to wish wish take1
	 */
	@Test
	public void testValidateAction_WISH_WISH_take_one() {
		Condition condition = Condition.WISH_WISH ;
		int gameMode = 1;  
		String currentWish = Suit.Clubs.toString();
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("take_one", 1);
		
		Condition actual = ruleService.validateAction(condition, gameMode, currentWish,playerChoice);
		
		Assert.assertEquals(Condition.WISH_WISH, actual);
	}
	
	/**
	 * Test validate action: gamemode 0,  skip to no effect  skip
	 */
	@Test
	public void testValidateAction_SKIP_skip() {
		Condition condition = Condition.SKIP;
		int gameMode = 1;  
		String currentWish = null;
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("skip", 1);
		
		Condition actual = ruleService.validateAction(condition, gameMode, currentWish,playerChoice);
		
		Assert.assertEquals(Condition.NO_EFFECT, actual);
	}
	
	/**
	 * Test validate action: gamemode 0,  skip skip to no effect  skipskip
	 */
	@Test
	public void testValidateAction_SKIP_SKIP_skip() {
		Condition condition = Condition.SKIP_SKIP;
		int gameMode = 1;
		String currentWish = Suit.Clubs.toString();
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("skip_skip", 1);
		
		Condition actual = ruleService.validateAction(condition, gameMode, currentWish,playerChoice);
		
		Assert.assertEquals(Condition.NO_EFFECT, actual);
	}
	
	/**
	 * Test validate action: gamemode 0,  plus2 to no effect  take2
	 */
	@Test
	public void testValidateAction_PLUS_TWO_take_two() {
		Condition condition = Condition.PLUS_TWO;
		int gameMode = 1;  
		String currentWish = null;
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("take_two", 1);
		
		Condition actual = ruleService.validateAction(condition, gameMode, currentWish,playerChoice);
		
		Assert.assertEquals(Condition.NO_EFFECT, actual);
	}
	
	/**
	 * Test validate action: gamemode 0,  plus2 strict to no effect  take2
	 */
	@Test
	public void testValidateAction_PLUS_TWO_STRICT_take_two() {
		Condition condition = Condition.PLUS_TWO_STRICT;
		int gameMode = 2;  
		String currentWish = null;
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("take_two", 1);
		
		Condition actual = ruleService.validateAction(condition, gameMode, currentWish,playerChoice);
		
		Assert.assertEquals(Condition.NO_EFFECT, actual);
	}
	
	/**
	 * Test validate action: gamemode 0,  plus4 to no effect  take4
	 */
	@Test
	public void testValidateAction_PLUS_FOUR_take_four() {
		Condition condition = Condition.PLUS_FOUR;
		int gameMode = 1;  
		String currentWish = null;
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("take_four", 1);
		
		Condition actual = ruleService.validateAction(condition, gameMode, currentWish,playerChoice);
		
		Assert.assertEquals(Condition.NO_EFFECT, actual);
	}
	
	@Test
	public void testHowMuchToTake_Plus2() {
		Condition condBefore = Condition.PLUS_TWO;
		Condition condAfter = Condition.NO_EFFECT;
		
		int result = ruleService.howMuchToTake(condBefore, condAfter);
		
		Assert.assertEquals(2,result);
	}
	
	@Test
	public void testHowMuchToTake_Plus1() {
		Condition condBefore = Condition.NO_EFFECT;
		Condition condAfter = Condition.NO_EFFECT;
		
		int result = ruleService.howMuchToTake(condBefore, condAfter);
		
		Assert.assertEquals(1,result);
	}
	
	
	
}

