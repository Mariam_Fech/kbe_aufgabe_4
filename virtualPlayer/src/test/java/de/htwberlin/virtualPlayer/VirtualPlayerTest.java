package de.htwberlin.virtualPlayer;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import de.htwberlin.deckService.domain.Card;
import de.htwberlin.deckService.domain.Rank;
import de.htwberlin.deckService.domain.Suit;
import de.htwberlin.deckService.impl.DeckService;
import de.htwberlin.virtualPlayer.impl.VirtualPlayer;


/**
 * Unit test for simple App.
 */
public class VirtualPlayerTest {
   
	/** The test bot has 1 Card */
	List<Card> botCards1;

	/** The test bot has 2 Cards */
	List<Card> botCards2;
	
	
	/** The test bot has 5 Cards */
	List<Card> botCards5;
	
	VirtualPlayer virtualPlayer;
	
	
	
	@Before
	public void setUp() {
		virtualPlayer = new VirtualPlayer();
		botCards1 = new ArrayList<Card>();
		botCards2 = new ArrayList<Card>();
		botCards5 = new ArrayList<Card>();
	
		botCards1.add(new Card(Suit.Spades, Rank.SIX));
		
		botCards2.add(new Card(Suit.Spades, Rank.SIX));
		botCards2.add(new Card(Suit.Clubs, Rank.King));
		
		botCards5 = new ArrayList<Card>();
		botCards5.add(new Card(Suit.Spades, Rank.SIX));
		botCards5.add(new Card(Suit.Clubs, Rank.King));
		botCards5.add(new Card(Suit.Diamonds, Rank.Jack));
		botCards5.add(new Card(Suit.Hearts, Rank.SEVEN));
		botCards5.add(new Card(Suit.Hearts, Rank.EIGHT));
	}
	/**
	 * Test bot choice action plus1, condition:no effect
	 */
	@Test
	public void testBotChoice_action_NO_EFFECT() {
		String condition = "NO_EFFECT"; 
		Card openCard = new Card(Suit.Hearts,Rank.TEN);
		String wish = null;
		Map<String, Integer> botChoice = virtualPlayer.botChoice(condition, botCards2, openCard, wish);
		
		Assert.assertTrue(botChoice.containsKey("take_one"));
		Assert.assertFalse(botChoice.containsKey("mau"));
	}
	/** 
	  * Test bot choice action plus4, condition:plus4
	 */
	@Test
	public void testBotChoice_action_PLUS_FOUR() {
		String condition = "PLUS_FOUR"; 
		Card openCard = new Card(Suit.Spades,Rank.SEVEN);
		String wish = null;
		
		Map<String, Integer> botChoice = virtualPlayer.botChoice(condition, botCards2, openCard, wish);
		
		Assert.assertTrue(botChoice.containsKey("take_four"));
		Assert.assertFalse(botChoice.containsKey("mau"));
	}
	
	/** 
	  * Test bot choice action plus2, condition:plus2strict
	 */
	@Test
	public void testBotChoice_action_PLUS_TWO_STRICT() {
		String condition = "PLUS_TWO_STRICT"; 
		Card openCard = new Card(Suit.Spades,Rank.SEVEN);
		String wish = null;
		
		Map<String, Integer> botChoice = virtualPlayer.botChoice(condition, botCards2, openCard, wish);
		
		Assert.assertTrue(botChoice.containsKey("take_two"));
		Assert.assertFalse(botChoice.containsKey("mau"));
	}
	
	/** 
	  * Test bot choice action skip mau, condition:skip
	 */
	@Test
	public void testBotChoice_action_SKIP_mau() {
		String condition = "SKIP"; 
		Card openCard = new Card(Suit.Spades,Rank.EIGHT);
		String wish = null;
		
		Map<String, Integer> botChoice = virtualPlayer.botChoice(condition, botCards1, openCard, wish);
		
		Assert.assertTrue(botChoice.containsKey("skip"));
		Assert.assertTrue(botChoice.containsKey("mau"));
	}
	
	/**
	 * Test bot choice card + mau, condition:no effect 
	 */
	@Test
	public void testBotChoice_card_NO_EFFECT_mau() {
		String condition = "NO_EFFECT"; 
		Card openCard = new Card(Suit.Clubs,Rank.ACE);
		String wish = null;
		
		Map<String, Integer> botChoice = virtualPlayer.botChoice(condition, botCards2, openCard, wish);
		
		Assert.assertTrue(botChoice.containsKey("card"));
		Assert.assertTrue(botChoice.containsValue(Integer.valueOf(2)));
		Assert.assertTrue(botChoice.containsKey("mau"));
	}
	/**
	 * Test bot choice card , condition:skip
	 */
	@Test
	public void testBotChoice_card_SKIP() {
		String condition = "SKIP"; 
		Card openCard = new Card(Suit.Clubs,Rank.EIGHT);
		String wish = null;
		
		Map<String, Integer> botChoice = virtualPlayer.botChoice(condition, botCards5, openCard, wish);
		
		Assert.assertTrue(botChoice.containsKey("card"));
		Assert.assertTrue(botChoice.containsValue(Integer.valueOf(5)));
		Assert.assertFalse(botChoice.containsKey("mau"));
	}
	/**
	 * Test bot choice card , condition:plus2
	 */
	@Test
	public void testBotChoice_card_PLUS_TWO() {
		String condition = "PLUS_TWO"; 
		Card openCard = new Card(Suit.Clubs,Rank.SEVEN);
		String wish = null;
		
		Map<String, Integer> botChoice = virtualPlayer.botChoice(condition, botCards5, openCard, wish);
		
		Assert.assertTrue(botChoice.containsKey("card"));
		Assert.assertTrue(botChoice.containsValue(Integer.valueOf(4)));
		Assert.assertFalse(botChoice.containsKey("mau"));
	}
	/**
	 * Test bot choice card jack, condition:wish
	 */
	@Test
	public void testBotChoice_card_WISH_jack() {
		String condition = "WISH"; 
		Card openCard = new Card(Suit.Clubs,Rank.Jack);
		String wish = Suit.Hearts.toString();
		
		Map<String, Integer> botChoice = virtualPlayer.botChoice(condition, botCards5, openCard, wish);
		
		Assert.assertTrue(botChoice.containsKey("card"));
		Assert.assertTrue(botChoice.containsValue(Integer.valueOf(3)));
		Assert.assertFalse(botChoice.containsKey("mau"));
	}
	/**
	 * Test bot choice card right suit, condition:wish
	 */
	@Test
	public void testBotChoice_card_WISH_suit() {
		String condition = "WISH"; 
		Card openCard = new Card(Suit.Clubs,Rank.Jack);
		String wish = Suit.Clubs.toString();
		
		Map<String, Integer> botChoice = virtualPlayer.botChoice(condition, botCards5, openCard, wish);
		
		Assert.assertTrue(botChoice.containsKey("card"));
		Assert.assertTrue(botChoice.containsValue(Integer.valueOf(2)));
		Assert.assertFalse(botChoice.containsKey("mau"));
	}
	/**
	 * Test bot choice card, condition:wish wish
	 */
	@Test
	public void testBotChoice_card_WISH_WISH() {
		String condition = "WISH_WISH"; 
		Card openCard = new Card(Suit.Clubs,Rank.Jack);
		String wish = Suit.Diamonds.toString();
		
		Map<String, Integer> botChoice = virtualPlayer.botChoice(condition, botCards5, openCard, wish);
		
		Assert.assertTrue(botChoice.containsKey("card"));
		Assert.assertTrue(botChoice.containsValue(Integer.valueOf(3)));
	}
	
	
	
	 /** 
	  * Test bot choice action plus2, condition:plus2
	 */
	@Test
	public void testBotChoice_action_PLUS_TWO() {
		String condition = "PLUS_TWO"; 
		Card openCard = new Card(Suit.Spades,Rank.SEVEN);
		String wish = null;
		
		Map<String, Integer> botChoice = virtualPlayer.botChoice(condition, botCards2, openCard, wish);
		
		Assert.assertTrue(botChoice.containsKey("take_two"));
		Assert.assertFalse(botChoice.containsKey("mau"));
	}
	
	
	
	/** 
	  * Test bot choice action skip_skip mau, condition:skip skip
	 */
	@Test
	public void testBotChoice_action_SKIP_SKIP_mau() {
		String condition = "SKIP_SKIP"; 
		Card openCard = new Card(Suit.Spades,Rank.EIGHT);
		String wish = null;
		
		Map<String, Integer> botChoice = virtualPlayer.botChoice(condition, botCards1, openCard, wish);
		
		Assert.assertTrue(botChoice.containsKey("skip_skip"));
		Assert.assertTrue(botChoice.containsKey("mau"));
	}
	/** 
	  * Test bot choice action take1 , condition:wish
	 */
	@Test
	public void testBotChoice_action_WISH() {
		String condition ="WISH"; 
		Card openCard = new Card(Suit.Spades,Rank.Jack);
		String wish = Suit.Diamonds.toString();
		
		Map<String, Integer> botChoice = virtualPlayer.botChoice(condition, botCards1, openCard, wish);
		
		Assert.assertTrue(botChoice.containsKey("take_one"));
		Assert.assertFalse(botChoice.containsKey("mau"));
	}
	/** 
	  * Test bot choice action take1, condition:wishwish
	 */
	@Test
	public void testBotChoice_action_WISH_WISH() {
		String condition = "WISH"; 
		Card openCard = new Card(Suit.Spades,Rank.Jack);
		String wish = Suit.Diamonds.toString();
		
		Map<String, Integer> botChoice = virtualPlayer.botChoice(condition, botCards2, openCard, wish);
		
		Assert.assertTrue(botChoice.containsKey("take_one"));
		Assert.assertFalse(botChoice.containsKey("mau"));
	}
	
	/** 
	  * Test bot choice action skip mau, condition:skip
	 */
	@Test
	public void testCreateBotWish() {
		List<Card> cards = new ArrayList<Card>();
		
		cards.add(new Card(Suit.Spades,Rank.EIGHT));
		
		
		String botWish = virtualPlayer.createBotWish(cards);
		
		Assert.assertEquals(botWish,Suit.Spades.toString());
		
	}
	
}
