package de.htwberlin.virtualPlayer.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import de.htwberli.virtualPlayer.inter.VirtualPlayerInt;
import de.htwberlin.deckService.domain.Card;
import de.htwberlin.deckService.domain.Rank;



@Service
public class VirtualPlayer implements VirtualPlayerInt{
   
	private static final Logger LOGGER = LogManager.getLogger(VirtualPlayer.class);
	
	@Override
	public String createBotWish(List<Card> cards) {
		if (cards.isEmpty()) {
			LOGGER.log(Level.INFO, "RuleService botWish empty deck");
			return null;
		}
		if (cards.size() == 1) {
			LOGGER.log(Level.INFO, "RuleService botWish one card");
			return cards.get(0).getSuit().toString();
		}
		List<String> suits = new ArrayList<String>();
		for (Card c : cards) {
			suits.add(c.getSuit().toString());
		}
		LOGGER.log(Level.INFO, "RuleService botWish suits of deck " + suits.toString());
		
		Random random = new Random();
		int randomNr = random.nextInt(suits.size() - 1);
		LOGGER.log(Level.INFO, "RuleService botWish cardNr. " + randomNr + " Suit " + suits.get(randomNr));
		return suits.get(randomNr);
	}
	

	/**
	 * Maps mau in map if decksize is two
	 * @param deckSize amount of cards in bots deck before move
	 * @param map HashMap for mapping
	 */
	private void botCheckMau(int deckSize, Map<String, Integer> map) {
		if (deckSize == 2)
			map.put("mau", 1);
	}

	/**
	 * Maps mau in map if decksize is one and action is not skip/skipskip
	 * @param deckSize deck size
	 * @param map HashMap for mapping
	 */
	private void botCheckMauWhenCardLeft(int deckSize, Map<String, Integer> map) {
		if (deckSize == 1 && (map.containsKey("skip") || map.containsKey("skip_skip"))) {
			map.put("mau", 1);
		}
	}
	@Override
	public Map<String,Integer> botChoice(String condition, List<Card> botCards, Card openCard, String wish) {
		Map<String,Integer> map = new HashMap<String,Integer>();

		int i = 0;
		for (Card c : botCards) {
			i++;
			if (condition.equals("WISH_WISH")) {
				if (c.getSuit().toString() == wish) {
					map.put("card", i);
					botCheckMau(botCards.size(), map);
					LOGGER.log(Level.INFO, "botChoice: wishwish " + map.toString());
					return map;
				}
			}
			if (condition.equals("WISH")) {
				if (c.getSuit().toString() == wish || c.getRank() == Rank.Jack) {
					map.put("card", i);
					botCheckMau(botCards.size(), map);
					LOGGER.log(Level.INFO, "botChoice: wish " + map.toString());
					return map;
				}
			}
			if (condition.equals("SKIP")) {
				if (c.getRank() == Rank.EIGHT) {
					map.put("card", i);
					botCheckMau(botCards.size(), map);
					LOGGER.log(Level.INFO, "botChoice: skip " + map.toString());
					return map;
				}
			}
			if (condition.equals("PLUS_TWO")) {
				if (c.getRank() == Rank.SEVEN) {
					map.put("card", i);
					botCheckMau(botCards.size(), map);
					LOGGER.log(Level.INFO, "botChoice: plus2 " + map.toString());
					return map;
				}
			}
			if (condition.equals("NO_EFFECT")) {
				if (c.getRank() == openCard.getRank() || c.getSuit() == openCard.getSuit()) {
					map.put("card", i);
					botCheckMau(botCards.size(), map);
					LOGGER.log(Level.INFO, "botChoice: noeffect " + map.toString());
					return map;
				}
			}
		}
		switch (condition) {
		case "NO_EFFECT":
			map.put("take_one", 1);
			break;
		case "WISH_WISH":
			map.put("take_one", 1);
			break;
		case "WISH":
			map.put("take_one", 1);
			break;
		case "SKIP":
			map.put("skip", 1);
			break;
		case "SKIP_SKIP":
			map.put("skip_skip", 1);
			break;
		case "PLUS_TWO":
			map.put("take_two", 1);
			break;
		case "PLUS_TWO_STRICT":
			map.put("take_two", 1);
			break;
		case "PLUS_FOUR":
			map.put("take_four", 1);
			break;
		case "INVALID_MOVE":
			break;
		}
		botCheckMauWhenCardLeft(botCards.size(), map);
		LOGGER.log(Level.INFO, "botChoice: action " + map.toString());
		return map;
	}
}
