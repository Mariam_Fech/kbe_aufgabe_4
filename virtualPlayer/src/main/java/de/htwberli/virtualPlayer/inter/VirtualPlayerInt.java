package de.htwberli.virtualPlayer.inter;


import java.util.List;
import java.util.Map;

import de.htwberlin.deckService.domain.Card;


public interface VirtualPlayerInt {

	/**
	 * Creates a bot wish as String name representation of a card suit
	 * @param cards List<Card> bots card List of Cards in bots deck
	 * @return String bots wish String representation of Card suit as bots wish
	 */
	String createBotWish(List<Card> cards);

	/**
	 * Evaluates bots Cards, current condition, optional wish  and open cards
	 * returns Bots choice as String -> Integer Mapping, with String "card" for a card to place
	 * or "action" for skipping or taking cards, 
	 * if there is only one card left after successful card placement, then String "mau" is mapped
	 * @param condition current Condition of the game
	 * @param botCards List of cards of bot
	 * @param openCard Card open card on the open deck
	 * @param wish String optional wish as String suit representation, if not set then null
	 * @return Map<String, Integer> Map with mapped values as bots choice
	 */
	Map<String, Integer> botChoice(String condition, List<Card> botCards, Card openCard, String wish);

	


	
}
