package de.htwberlin.deckService;

import org.junit.Test;

import de.htwberlin.deckService.domain.Card;
import de.htwberlin.deckService.domain.Deck;
import de.htwberlin.deckService.domain.Rank;
import de.htwberlin.deckService.domain.Suit;
import de.htwberlin.deckService.impl.DeckService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;

/**
 *Test class to test DeckService-Implementation
 *
 */
public class DeckServiceTest {
	
	DeckService deckservice;
	Deck deckClosed;
	Deck deckOpen;
	
	
	/**
	 * Performs a Set Up to prepare the environment for the tests
	 */
	@Before
	public void setUp() {
		deckservice = new DeckService();
		deckClosed = new Deck();
		deckOpen = new Deck();
		deckClosed.getCards().add(new Card(Suit.Hearts, Rank.SIX));
		deckClosed.getCards().add(new Card(Suit.Hearts, Rank.SEVEN));
		deckClosed.getCards().add(new Card(Suit.Diamonds, Rank.EIGHT));
		deckClosed.getCards().add(new Card(Suit.Diamonds, Rank.NINE));
		deckClosed.getCards().add(new Card(Suit.Clubs, Rank.TEN));
		deckClosed.getCards().add(new Card(Suit.Clubs, Rank.Jack));
		deckClosed.getCards().add(new Card(Suit.Spades, Rank.King));
		deckClosed.getCards().add(new Card(Suit.Spades, Rank.ACE));	
	}
	
	/**
	 * Tests if the generated deck contains the right amount of cards
	 */
	@Test
	public void testGenerateDeckAmountAndCards() {
		Deck d = new Deck();
		int expected = 36;
		Card cardFirst = new Card(Suit.Spades,Rank.SIX);
		Card cardLast = new Card(Suit.Clubs,Rank.ACE);

		d= deckservice.generateDeck();
		int countCards = d.getCards().size();
		
		Assert.assertEquals(expected, countCards);
		Assert.assertEquals(cardFirst.toString(),d.getCards().get(0).toString());
		Assert.assertEquals(cardLast.toString(),d.getCards().get(d.getCards().size()-1).toString());
	}
	
	/**
	 * Tests if the deck is being shuffled 
	 * and the resulting deck is not equal to the initial one
	 */
	@Test
	public void testIfCardsAreMixed() {
		Deck deck = new Deck();
		Iterator<Card> iterator = deckClosed.getCards().iterator();
		while(iterator.hasNext()){
		    deck.getCards().add((Card) iterator.next());  
		}
		
		deckservice.mixCards(deck);
		
		Assert.assertNotEquals(deckClosed, deck);
	}
	
	/**
	 * Test if the deck is turned around after it was emptied 
	 * and the open card is still open
	 */
	@Test
	public void testTurnAroundDeckWhenEmpty() {
		Card c = deckClosed.getCards().get(deckClosed.getCards().size()-1);
		
		deckservice.turnAroundDeck(deckOpen,deckClosed);
		
		Assert.assertEquals(7,deckOpen.getCards().size());
		Assert.assertEquals(1, deckClosed.getCards().size());
		Assert.assertEquals(c, deckClosed.getCards().get(0));
		
	}
	
	/**
	 * Tests if the first card is turned around, meaning one card is taken from 
	 * deckClosed and put into deckOpen
	 */
	@Test
	public void testTernAroundFirstCard() {
		
		deckservice.turnFirstCard(deckClosed, deckOpen);
		Assert.assertEquals(7,deckClosed.getCards().size());
		Assert.assertEquals(1, deckOpen.getCards().size());
	}
	
	/**
	 * Tests if the empty deck is detected, returning true
	 * and not empty deck returning false
	 */
	@Test
	public void testAlertEmptyDeck() {
		Assert.assertTrue(deckservice.alertEmptyDeck(deckOpen));
		Assert.assertFalse(deckservice.alertEmptyDeck(deckClosed));
	}
	
	@Test
	public void testCardChangeDeck() {
		String topCardInDeck = deckClosed.getCards().get(deckClosed.getCards().size()-1).toString();
		deckservice.cardChangeDeck(deckClosed, deckOpen);
		
		Assert.assertEquals(1,deckOpen.getCards().size());
		Assert.assertEquals(7,deckClosed.getCards().size());
		Assert.assertEquals(topCardInDeck, deckOpen.getCards().get(0).toString());
	}
	
	@Test
	public void testCountScoreOfDeck() {
		int scoreExpexted= 85;
		int scoreActual = deckservice.countScore(deckClosed);
		Assert.assertEquals(scoreExpexted, scoreActual);
	}
	
	@Test
	public void testGetCardsAsStringList() {
		List<String> expected = new ArrayList<String>();
		expected.add("Hearts  SIX");expected.add("Hearts  SEVEN");
		expected.add("Diamonds  EIGHT");expected.add("Diamonds  NINE");
		expected.add("Clubs  TEN");expected.add("Clubs  Jack");
		expected.add("Spades  King");expected.add("Spades  ACE");
		
		List<String> actual = deckservice.getCardsAsStringList(deckClosed);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testGetOpenCard() {
		Card expected = new Card(Suit.Spades,Rank.ACE);
		
		Card actual = deckservice.getOpenCard(deckClosed);
		
		Assert.assertEquals(expected.toString(), actual.toString());
	
	}
}
