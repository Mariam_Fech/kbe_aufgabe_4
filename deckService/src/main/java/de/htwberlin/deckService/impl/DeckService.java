package de.htwberlin.deckService.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import de.htwberlin.deckService.domain.Card;
import de.htwberlin.deckService.domain.Deck;
import de.htwberlin.deckService.domain.Rank;
import de.htwberlin.deckService.domain.Suit;
import de.htwberlin.deckService.inter.DeckServiceInt;

/**
 * The Class DeckService.
 */
@Service
public class DeckService implements DeckServiceInt {
	
	private static final Logger LOGGER = LogManager.getLogger(DeckService.class);

	
	@Override
	public Deck generateDeck() {
		Deck deck = new Deck();
		Suit[] suit = Suit.values();
		Rank[] rank = Rank.values();
		for (Suit s : suit) {
			for (Rank r : rank) {
				Card c = new Card(s, r);
				deck.getCards().add(c);
			}
		}
		LOGGER.log(Level.INFO, "generateDeck deck " + deck.getCards().toString());
		return deck;

	}
	
	@Override
	public void mixCards(Deck deck) {
		Collections.shuffle(deck.getCards());
		LOGGER.log(Level.INFO, "mixCards end");
	}

	@Override
	public boolean alertEmptyDeck(Deck deck) {
		LOGGER.log(Level.INFO, "alertEmptyDeck");
		return deck.getCards().isEmpty();
	}

	@Override
	public void turnAroundDeck(Deck deckEmpty, Deck deckFull) {
		LOGGER.log(Level.INFO, "turnAroundDeck start");
		Card temp = deckFull.getCards().remove(deckFull.getCards().size() - 1);

		Iterator<Card> iterator = deckFull.getCards().iterator();
		while (iterator.hasNext()) {
			deckEmpty.getCards().add((Card) iterator.next());
		}
		deckFull.getCards().clear();
		deckFull.getCards().add(temp);
		mixCards(deckEmpty);
		LOGGER.log(Level.INFO, "turnAroundDeck end");
	}

	@Override
	public Card turnFirstCard(Deck deck, Deck openDeck) {
		Card openCard = deck.getCards().remove(deck.getCards().size() - 1);
		openDeck.getCards().add(openCard);
		LOGGER.log(Level.INFO, "turnFirstCard opencard "+openCard.toString());
		return openCard;
	}
	
	@Override
	public Card cardChangeDeck(Deck deckOut, Deck deckIn) {
		int lastIndex = deckOut.getCards().size() - 1;
		Card c = deckOut.getCards().remove(lastIndex);
		deckIn.getCards().add(c);
		LOGGER.log(Level.INFO, "cardChangeDeck card "+c.toString());
		return c;
	}

	@Override
	public List<String> getCardsAsStringList(Deck deck) {
		List<Card> cards = deck.getCards();
		List<String> list = new ArrayList<String>();
		for (Card c : cards) {
			list.add(c.toString());
		}
		LOGGER.log(Level.INFO, "getCardsAsStringList list "+list.toString());
		return list;
	}

	@Override
	public Card getOpenCard(Deck deck) {
		Card c = deck.getCards().get(deck.getCards().size() - 1);
		LOGGER.log(Level.INFO, "getOpenCard opencard "+c.toString());
		return c;
	}

	
	@Override
	public Integer countScore(Deck deck) {
		Integer result = 0;
		for (Card c : deck.getCards()) {
			Rank rank = c.getRank();
			switch (rank) {
			case SIX:
				result += 6;
				break;
			case SEVEN:
				result += 7;
				break;
			case EIGHT:
				result += 8;
				break;
			case NINE:
				result += 9;
				break;
			case TEN:
				result += 10;
				break;
			case Jack:
				result += 20;
				break;
			case Queen:
				result += 11;
				break;
			case King:
				result += 12;
				break;
			case ACE:
				result += 13;
				break;
			}
		}
		LOGGER.log(Level.INFO, "countScore count "+result.toString());
		return result;
	}

}
