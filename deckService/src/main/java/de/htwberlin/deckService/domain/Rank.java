package de.htwberlin.deckService.domain;

public enum Rank {
	SIX, SEVEN, EIGHT, NINE, TEN, Jack, Queen, King, ACE
}
