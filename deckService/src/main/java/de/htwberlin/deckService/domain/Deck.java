package de.htwberlin.deckService.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;



// TODO: Auto-generated Javadoc
/**
 * The Class Deck.
 */
@Entity
@Table(name = "Deck")
public class Deck {
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	@Column(name = "deckId", unique = true)
	private int id;
	
	/** The cards. */
	@OneToMany(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name = "deckId")
	@OrderColumn
	private List<Card> cards;
	
	/**
	 * Instantiates a new deck.
	 */
	public Deck() {
		cards = new ArrayList<Card>();
	}
	
	/**
	 * Sets the cards.
	 *
	 * @param cards the new cards
	 */
	public void setCards(List<Card> cards) {
		this.cards = cards;
	}

	/**
	 * Gets the cards.
	 *
	 * @return the cards
	 */
	public List<Card> getCards() {
		return cards;
	}

}
