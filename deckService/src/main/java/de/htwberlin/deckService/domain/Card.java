package de.htwberlin.deckService.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Card")
public class Card {
	/** The Id. */
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	@Column(name = "cardId")
	private Integer id;
	
	/** The suit. */
	@Enumerated(EnumType.STRING)
	private Suit suit;
	
	/** The rank. */
	@Enumerated(EnumType.STRING)
	private Rank rank;
	
	/**
	 * Instantiates a new card.
	 *
	 * @param suit the suit
	 * @param rank the rank
	 */
	public Card(Suit suit, Rank rank) {
		this.suit = suit;
		this.rank = rank;
	}
	
	/**
	 * Gets the suit.
	 *
	 * @return the suit of the card
	 */
	public Suit getSuit() {
		return suit;
	}

	/**
	 * Getter method for rank
	 * @return rank of the card
	 */
	public Rank getRank() {
		return rank;
	}

	@Override
	public String toString() {
		return suit + "  " + rank;
	}
	
	
}
