package de.htwberlin.deckService.inter;

import java.util.List;

import de.htwberlin.deckService.domain.Card;
import de.htwberlin.deckService.domain.Deck;
import de.htwberlin.deckService.domain.Rank;
import de.htwberlin.deckService.domain.Suit;

// TODO: Auto-generated Javadoc
/**
 * The Interface DeckServiceInt.
 */
public interface DeckServiceInt {
	
	/**
	 * Generate deck of 36 cards(six-ace)
	 * @return Deck filled deck
	 */
	public Deck generateDeck() ;	

	/**
	 * Mix / shuffle cards in deck
	 * @param deck deck to be shuffled
	 */
	public void mixCards(Deck deck);
	
	/**
	 * Alert empty deck.
	 * @param deck the deck to be checked if it is empty
	 * @return true if deck is empty
	 */
	public boolean alertEmptyDeck(Deck deck);
	
	/** 
	 * Places last card of deckOut into DeckIn
	 * @param deckOut deck to take card from
	 * @param deckIn deck to put card into
	 * @return Card the card that changed its deck
	 */
	public Card cardChangeDeck(Deck deckOut, Deck deckIn);
	
	/**
	 * Turn the deck around and leave the last open card in the open deck.
	 * @param deckEmpty deck to be filled
	 * @param deckFull deck to take out cards from (except of the last one)
	 */
	public void turnAroundDeck(Deck deck, Deck openDeck);
	
	/**
	 * First card of covered deck is placed into open deck and this card is returned.
	 * @param deck covered deck
	 * @param openDeck open deck
	 * @return card open card on open deck
	 */
	public Card turnFirstCard(Deck deck, Deck openDeck);
	
	/** 
	 * Returns the score of a deck
	 * Value of Ranks: six=6, seven=7,..ten=10, jack=20, queen=11, king=12, ace=13
	 * @param deck the deck to count score of
	 * @return Integer result value of the deck
	 */
	public Integer countScore(Deck deck);

	/**
	 * Converts the Cards of the Deck into Strings, puts into a List and returns this
	 * @param deck source of cards
	 * @return List<String> list of Cards converted to Strings
	 */
	public List<String> getCardsAsStringList(Deck deck);

	/** 
	 * Returns the open card of open deck = last card in deck
	 * @param deck open deck to get the last card from
	 * @return Card open card
	 */
	public Card getOpenCard(Deck deck);
	
	
}
