package de.htwberlin.gameService.domain;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import de.htwberlin.deckService.domain.Deck;
//
/**
 * The Class Player.
 */
@Entity
@Table(name = "Player")
public class Player {
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	@Column(name = "playerId", unique = true)
	private int id;

	/** The nickname. */
	private String nickname;
	
	/** The playerdeck. */
	@OneToOne(cascade = CascadeType.ALL)
	private Deck pDeck;
	
	private boolean mau;
	
	private boolean wish;
	
	private int countWins;
	
	
	/**
	 * Instantiates a new player.
	 *
	 * @param nickname
	 */
	public Player(String nickname) {
		this.nickname = nickname;
		this.pDeck = new Deck();
		this.mau = false;
		this.wish=false;
		this.countWins=0;
	}
	
	public boolean isWish() {
		return wish;
	}

	public void setWish(boolean wish) {
		this.wish = wish;
	}

	/**
	 * Gets the nickname.
	 *
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}
	
	/**
	 * Sets the nickname.
	 *
	 * @param nickname the new nickname
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	
	/**
	 * Gets the players deck.
	 *
	 * @return the players deck
	 */
	public Deck getpDeck() {
		return pDeck;
	}
	
	public void setpDeck(Deck pDeck) {
		this.pDeck = pDeck;
	}
	
	public boolean isMau() {
		return mau;
	}

	public void setMau(boolean mau) {
		this.mau = mau;
	}

	public int getCountWins() {
		return this.countWins;
	}

	public void setCountWins(int wins) {
		this.countWins=wins;
		
			
	}
	
	
}
