package de.htwberlin.gameService.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import de.htwberlin.deckService.domain.Deck;
import de.htwberlin.ruleService.domain.Condition;

// TODO: Auto-generated Javadoc
//
/**
 * The Class Game.
 */
@Entity
@Table(name = "Game")
public class Game {
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	@Column(name = "gameId", unique = true)
	private int id;

	/** The deck open. */
	@OneToOne(cascade = CascadeType.ALL)
	private Deck deckOpen = null;
	
	/** The deck closed. */
	@OneToOne(cascade = CascadeType.ALL)
	private Deck deckClosed = null;

	/** The players. */
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "gameId")
	private List<Player> players = null;
	
	/** The whos turn. */
	private int whosTurn;
	
	/** The condition. */
	@Enumerated(EnumType.STRING)
	private Condition cond;
	
	/** The current wish. */
	private String currentWish;
	
	/** The gameMode. */
	private int gameMode;
	
	/** The winner. */
	@OneToOne(cascade = CascadeType.ALL)
	private Player winner;
		
	
	/**
	 * Instantiates a new game.
	 */
	public Game() {
		this.cond = Condition.NO_EFFECT;
		this.deckClosed = new Deck();
		this.deckOpen = new Deck();
		this.players = new ArrayList<Player>();
		this.whosTurn = 0;
		this.currentWish = null;
		this.gameMode = 0; 
		this.winner = null;
		
	}
	
	/**
	 * Gets the condition.
	 *
	 * @return the condition
	 */
	public Condition getCondition() {
		return cond;
	}

	/**
	 * Sets the condition.
	 *
	 * @param condition the new condition
	 */
	public void setCondition(Condition condition) {
		this.cond = condition;
		switch(condition) {
		case NO_EFFECT :this.currentWish=null;break;
		case PLUS_TWO: this.currentWish=null;break;
		case PLUS_TWO_STRICT:this.currentWish=null;break;
		case PLUS_FOUR:this.currentWish=null;break;
		case SKIP:this.currentWish=null;break;
		case SKIP_SKIP: this.currentWish=null;break;
		default: break;
		}	
	}
		
	

	/**
	 * Gets the open deck.
	 *
	 * @return the deck open
	 */
	public Deck getDeckOpen() {
		return deckOpen;
	}
	
	/**
	 * Gets the closed deck.
	 *
	 * @return the deck closed
	 */
	public Deck getDeckClosed() {
		return deckClosed;
	}
	
	
	/**
	 * Gets the players.
	 *
	 * @return the players
	 */
	public List<Player> getPlayers() {
		return players;
	}

	/**
	 * Gets whosTurn.
	 *
	 * @return the whos turn
	 */
	public int getWhosTurn() {
		return whosTurn;
	}

	/**
	 * Sets whosTurn.
	 *
	 * @param whosTurn the new whos turn
	 */
	public void setWhosTurn(int whosTurn) {
		this.whosTurn = whosTurn;
	}
	
	public String getCurrentWish() {
		return currentWish;
	}

	public void setCurrentWish(String currentWish) {
		this.currentWish = currentWish;
	}

	public int getGameMode() {
		return gameMode;
	}

	public void setGameMode(int gameMode) {
		this.gameMode = gameMode;
	}

	public void setDeckOpen(Deck deckOpen) {
		this.deckOpen = deckOpen;
	}

	public void setDeckClosed(Deck deckClosed) {
		this.deckClosed = deckClosed;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public Player getWinner() {
		return winner;
	}

	public void setWinner(Player winner) {
		this.winner = winner;
	}
	
	
}
