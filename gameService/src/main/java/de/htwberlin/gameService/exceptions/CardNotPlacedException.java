package de.htwberlin.gameService.exceptions;

public class CardNotPlacedException extends RuntimeException{

	private static final long serialVersionUID = 1L;

		public CardNotPlacedException(String message) {
			super(message);
		}

}
