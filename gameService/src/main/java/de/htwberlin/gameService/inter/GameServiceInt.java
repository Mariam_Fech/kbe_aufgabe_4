package de.htwberlin.gameService.inter;


import java.util.List;
import java.util.Map;

import de.htwberlin.deckService.domain.Card;
import de.htwberlin.gameService.domain.Game;
import de.htwberlin.gameService.domain.Player;
import de.htwberlin.gameService.exceptions.CardNotPlacedException;
import de.htwberlin.gameService.exceptions.InvalidMoveException;


public interface GameServiceInt{

	/**
	 * Sets up the game: sets users nickname, gamemode
	 * generates new deck, mixes deck, hands out 5 cards to every player
	 * turns around first card
	 * @param game
	 * @param playerName
	 * @param gameMode gamemode 1=1xfight back on action cards allowed, 2=strict rules 
	 * @param bots number of bots wished by user
	 */
	public void initGame(Game game, String playerName, int gameMode, int bots);
	
	/**
	 * Gets the count of bots cards
	 * @param game
	 * @return List<Integer> list with amounts of cards of the bots
	 */
	public List<Integer> countBotCards(Game game);

	/**
	 * Gets players cards as List<String>
	 * @param game
	 * @return List<String> the list with the cards as strings
	 */
	public List<String> getPlayerCards(Game game);
	
	/**
	 * Gets the last=open card of the open deck
	 * @param game
	 * @return
	 */
	public Card getOpenCard(Game game);

	/**
	 * Places bots move, returns true if move is allowed and successful
	 * @param game
	 * @param playerChoice
	 * @return true if successful
	 * @throws InvalidMoveException 
	 * @throws CardNotPlacedException 
	 */
	public boolean playerMove(Game game, Map<String, Integer> playerChoice) throws InvalidMoveException, CardNotPlacedException;
	
	/**
	 * Places bots move, returns true if move is allowed and successful
	 * @param game
	 * @return true if successful
	 * @throws InvalidMoveException 
	 * @throws CardNotPlacedException 
	 */
	public boolean botMove(Game game) throws InvalidMoveException, CardNotPlacedException;

	/**
	 * Sets whos turn to the next player
	 * @param game
	 * @return int number of player in game whos turn it is
	 */
	public int nextPlayer(Game game);

	/**
	 * Counts for every player the score of his deck, maps and returns the map
	 * @param game
	 * @return Map<String,Integer> map for mapping the scores of players decks
	 */
	public Map<String, Integer> countScoreGame(Game game);

	/**
	 * Sets users wish (suit)
	 * @param game
	 * @param int currentWish: 1=hearts, 2=diamonds, 3=clubs, 4=spades
	 */
	public void setCurrentWishPlayer(Game game, int currentWish);

	/**
	 * Sets bots wished suit
	 * @param game
	 * @param player p bot
	 */
	public void setCurrentWishBot(Game game, Player p);

	/**
	 * Checks if player has one card and mau is set puts punishment
	 * @param game
	 * @param player
	 * @return String 	"nomau" if one card and no mau, 
	 * 					"evalmau" if more than one card and mau
	 * 					"mau" if one card and mau
	 * 					"winner" if player has no more cards
	 */
	public String checkCardsAndMau(Game game, Player player);

	/**
	 * Sets for all players of the game isMau to false
	 * @param game
	 */
	public void unsetMau(Game game);

	/**
	 * Adds a player to the game
	 * @param game
	 * @param player
	 */
	public void addPlayer(Game game, Player p);

	/**
	 * Puts 5 cards from deck to every players deck
	 * @param game
	 * @param players
	 * @return true if success
	 */
	public boolean handOut(Game game, List<Player> players);

	/**
	 * Puts one Card from closed deck to players deck
	 * @param game
	 * @param player
	 * @return Card that was taken from closed deck
	 */
	public Card takeCard(Game game, Player player);

	/**
	 * Makes player take 2 cards from closed deck 
	 * as punishment for forgetting mau or using it to wrong time
	 * @param game
	 * @param playerNr
	 */
	public void punishMau(Game game, int playerNr);

	/**
	 * Returns bots wish of suit as string
	 * @param p bot
	 * @return String bots wish suit
	 */
	public String botWish(Player p);

	/**
	 * Puts players choice from his deck to open deck
	 * @param game
	 * @param player p
	 * @param myCard players card choice
	 * @return true if card was placed
	 */
	public boolean placeCard(Game game, Player p, Card myCard) throws CardNotPlacedException;

	/**
	 * Sets players isWish false
	 * @param game
	 */
	public void setPlayersWishFalse(Game game);

	
	/**
	 * Checks if the Player put a wish card and by that put his last card and win, sets him as winner
	 * @param game Game : the game
	 * @param player Player :the player who's turn is now
	 * @return
	 */
	boolean checkWishAndWinner(Game game, Player player);





	
	
	
	
	
}
