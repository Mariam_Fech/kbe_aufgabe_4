package de.htwberlin.gameService.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.htwberlin.deckService.domain.Card;
import de.htwberlin.deckService.domain.Deck;
import de.htwberlin.deckService.domain.Suit;
import de.htwberlin.deckService.impl.DeckService;
import de.htwberlin.gameService.domain.Game;
import de.htwberlin.gameService.domain.Player;
import de.htwberlin.gameService.exceptions.CardNotPlacedException;
import de.htwberlin.gameService.exceptions.InvalidMoveException;
import de.htwberlin.gameService.inter.GameServiceInt;
import de.htwberlin.ruleService.domain.Condition;
import de.htwberlin.ruleService.impl.RuleService;
import de.htwberlin.virtualPlayer.impl.VirtualPlayer;

@Service
public class GameService implements GameServiceInt {

	private VirtualPlayer virtualPlayer; 
	
	@Autowired
	public void setVirtualPlayer(VirtualPlayer virt) {
		this.virtualPlayer = virt;
	}
	private RuleService ruleService;
	
	@Autowired
	public void setRuleService(RuleService rule) {
		this.ruleService = rule;
	}
	
	private DeckService deckService;
	
	@Autowired
	public void setDeckService(DeckService deckService) {
		this.deckService = deckService;
	}
	
	public DeckService getDeckService() {
		return deckService;
	}
	
	public RuleService getRuleService() {
		return ruleService;
	}
	
	

	
	private static final Logger LOGGER = LogManager.getLogger(GameService.class);
	
	

	@Override
	public void initGame(Game game, String playerName, int gameMode, int bots) {
		LOGGER.log(Level.INFO, "GameService initGame startet");
		addPlayer(game, new Player(playerName));
		for(int i=0; i<bots;i++) {
		addPlayer(game, new Player("Bot "+(i+1)));
		}
		game.setGameMode(gameMode);
		game.setDeckClosed(deckService.generateDeck());
		deckService.mixCards(game.getDeckClosed());
		handOut(game, game.getPlayers());
		deckService.turnFirstCard(game.getDeckClosed(), game.getDeckOpen());

		LOGGER.log(Level.INFO, "GameService initGame finished");
	}

	@Override
	public boolean playerMove(Game game, Map<String, Integer> playerChoice) throws InvalidMoveException, CardNotPlacedException{
		Player player = game.getPlayers().get(game.getWhosTurn());
		LOGGER.log(Level.INFO, "GameService playerMove " + player.getNickname());

		if (playerChoice.containsKey("card")) {
			LOGGER.log(Level.INFO, "GameService playerMove card");
			Card openCard = getOpenCard(game);
			Card card = player.getpDeck().getCards().get(playerChoice.get("card") - 1);
			String currentWish = game.getCurrentWish();
			int gameMode = game.getGameMode();
			Condition currentCondition = game.getCondition();

			Condition condition = ruleService.validateCard(currentCondition, gameMode, currentWish, openCard, card);

			if (condition == Condition.INVALID_MOVE || condition == null) {
				LOGGER.log(Level.INFO, "GameService playerMove invalid move");
				throw new InvalidMoveException("card not allowed");
			}
			game.setCondition(condition);
			placeCard(game, player, card);
			if (condition == Condition.WISH || condition == Condition.WISH_WISH) {
				player.setWish(true);
				LOGGER.log(Level.INFO, "GameService playerMove wish");
			}
			if (playerChoice.containsKey("mau")) {
				LOGGER.log(Level.INFO, "GameService playerMove mau");
				player.setMau(true);
			}
			LOGGER.log(Level.INFO, "GameService playerMove ok");
			return true;
		} 
		
		else {
			LOGGER.log(Level.INFO, "GameService playerMove condition");
			Condition condBefore = game.getCondition();

			Condition condAfter = ruleService.validateAction(game.getCondition(), game.getGameMode(),
					game.getCurrentWish(), playerChoice);

			if (condAfter == Condition.INVALID_MOVE) {
				LOGGER.log(Level.INFO, "GameService playerMove invalid move");
				throw new InvalidMoveException("action not allowed");
			} 
			else {
				int cardsToTake = ruleService.howMuchToTake(condBefore,condAfter);
				while(cardsToTake>0) {
					takeCard(game,player);
					cardsToTake--;
				}
				game.setCondition(condAfter);

				if (playerChoice.containsKey("mau")) {
					LOGGER.log(Level.INFO, "GameService playerMove mau");
					player.setMau(true);
				}
				LOGGER.log(Level.INFO, "GameService playerMove ok");
				return true;
			}
		}
	}

	@Override
	public boolean botMove(Game game) throws InvalidMoveException, CardNotPlacedException{

		int botNr = game.getWhosTurn();
		Card openCard = getOpenCard(game);
		List<Card> botCards = game.getPlayers().get(botNr).getpDeck().getCards();
		Condition condition = game.getCondition();
		String wish = game.getCurrentWish();
		String cond =condition.toString();

		Map<String,Integer> map = virtualPlayer.botChoice(cond, botCards, openCard, wish);

		boolean ok = playerMove(game, map);

		LOGGER.log(Level.INFO, "GameService botMove ok = " + ok);

		return ok;

	}

	@Override
	public int nextPlayer(Game game) {
		int i = game.getWhosTurn();
		LOGGER.log(Level.INFO, "GameService nextPlayer: vorher" + i);
		// System.out.println("gameService next Player() wer bis jetzt dran war " + i);
		if (game.getWhosTurn() < game.getPlayers().size() - 1) {
			game.setWhosTurn(game.getWhosTurn() + 1);
		} else
			game.setWhosTurn(0);
		int y = game.getWhosTurn();
		LOGGER.log(Level.INFO, "GameService nextPlayer: nachher" + y);
		// System.out.println("gameService next Player() wer jetzt dran ist " + y);
		return y;
	}

	@Override
	public Map<String, Integer> countScoreGame(Game game) {
		Map<String, Integer> scores = new HashMap<String, Integer>();
		for (Player p : game.getPlayers()) {
			scores.put(p.getNickname(), deckService.countScore(p.getpDeck()));
		}
		LOGGER.log(Level.INFO, "GameService countScoreGame: " + scores.toString());
		return scores;
	}

	@Override
	public List<Integer> countBotCards(Game game) {
		List<Integer> counts = new ArrayList<Integer>();
		for (int i = 1; i < game.getPlayers().size(); i++) {
			counts.add(game.getPlayers().get(i).getpDeck().getCards().size());
		}
		return counts;
	}

	@Override
	public List<String> getPlayerCards(Game game) {
		Deck deck = game.getPlayers().get(0).getpDeck();
		return deckService.getCardsAsStringList(deck);
	}

	@Override
	public Card getOpenCard(Game game) {
		return deckService.getOpenCard(game.getDeckOpen());

	}

	@Override
	public void addPlayer(Game game, Player p) {
		game.getPlayers().add(p);
	}

	@Override
	public boolean handOut(Game game, List<Player> players) {
		for (int i = 0; i < 5; i++) {
			for (Player p : players) {
				takeCard(game, p);
			}
		}
		return true;
	}

	@Override
	public Card takeCard(Game game, Player player) {
		LOGGER.log(Level.INFO, "GameService takeCard start");
		Deck deckClosed = game.getDeckClosed();
		Deck deckOpen = game.getDeckOpen();
		Deck playerDeck = player.getpDeck();
		if (deckService.alertEmptyDeck(deckClosed)) {
			deckService.turnAroundDeck(deckClosed, deckOpen);
		}
		if (player.isMau()) {
			player.setMau(false);
			LOGGER.log(Level.INFO, "GameService takeCard mau unset");
		}
		return deckService.cardChangeDeck(deckClosed, playerDeck);
	}

	@Override
	public boolean placeCard(Game game, Player p, Card myCard) throws CardNotPlacedException {
		boolean remove = p.getpDeck().getCards().remove(myCard);
		boolean add = game.getDeckOpen().getCards().add(myCard);
		if (remove && add) {
			LOGGER.log(Level.INFO, "GameService placeCard ok");
			return true;
		} else {
			LOGGER.log(Level.INFO, "GameService placeCard failed");
			//return false;
			throw new CardNotPlacedException("card removed from players deck:"+remove+", card added to deck: "+add);
		}
	}

	@Override
	public void setCurrentWishBot(Game game, Player p) {
		game.setCurrentWish(botWish(p));
		setPlayersWishFalse(game);
		LOGGER.log(Level.INFO, "GameService setCurrentBotWish end");
	}

	@Override
	public String botWish(Player p) {
		LOGGER.log(Level.INFO, "GameService botWish start");
		return virtualPlayer.createBotWish(p.getpDeck().getCards());
	}

	@Override
	public void setCurrentWishPlayer(Game game, int currentWish) {
		LOGGER.log(Level.INFO, "GameService setCurrentWishPlayer " + currentWish);
		switch (currentWish) {
		case (1):
			game.setCurrentWish(Suit.Hearts.toString());
			break;
		case (2):
			game.setCurrentWish(Suit.Diamonds.toString());
			break;
		case (3):
			game.setCurrentWish(Suit.Clubs.toString());
			break;
		case (4):
			game.setCurrentWish(Suit.Spades.toString());
			break;
		}
		setPlayersWishFalse(game);
	}

	@Override
	public void setPlayersWishFalse(Game game) {
		LOGGER.log(Level.INFO, "GameService setPlayerWishFalse");
		for (Player p : game.getPlayers()) {
			p.setWish(false);
		}

	}

	@Override
	public String checkCardsAndMau(Game game, Player player) {
		LOGGER.log(Level.INFO, "GameService checkCardsAndMau start");
		int cards = player.getpDeck().getCards().size(); 
		int playerNr = game.getWhosTurn();
		
		String result = ruleService.checkCardsAndMau(cards, player.isMau());
		
		if(result.equals("nomau") || result.equals("evalmau"))
			punishMau(game, playerNr);
		
		if (result.equals("winner")) 
			game.setWinner(player);
		LOGGER.log(Level.INFO, "GameService checkCardsAndMau result " + result);
		return result;
	}

	@Override
	public void punishMau(Game game, int playerNr) {
		LOGGER.log(Level.INFO, "GameService punishMau");
		Player player = game.getPlayers().get(playerNr);
		takeCard(game, player);
		takeCard(game, player);
	}

	@Override
	public void unsetMau(Game game) {
		LOGGER.log(Level.INFO, "GameService unsetMau");
		for (Player p : game.getPlayers()) {
			if (p.getpDeck().getCards().size() > 1) {
				p.setMau(false);
			}
		}
	}
	@Override
	public boolean checkWishAndWinner(Game game, Player player) {
			if (player.getpDeck().getCards().isEmpty()) {
				game.setWinner(player);
				return true;
			} else 
				return false;
	}
}
