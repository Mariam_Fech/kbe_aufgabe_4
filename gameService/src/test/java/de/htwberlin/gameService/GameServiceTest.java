package de.htwberlin.gameService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import de.htwberlin.deckService.domain.Card;
import de.htwberlin.deckService.domain.Deck;
import de.htwberlin.deckService.domain.Rank;
import de.htwberlin.deckService.domain.Suit;
import de.htwberlin.deckService.impl.DeckService;
import de.htwberlin.gameService.domain.Game;
import de.htwberlin.gameService.domain.Player;
import de.htwberlin.gameService.exceptions.CardNotPlacedException;
import de.htwberlin.gameService.exceptions.InvalidMoveException;
import de.htwberlin.gameService.impl.GameService;
import de.htwberlin.ruleService.domain.Condition;
import de.htwberlin.ruleService.impl.RuleService;
import de.htwberlin.virtualPlayer.impl.VirtualPlayer;

// TODO: Auto-generated Javadoc
/**
 * The Class GameServiceTest.
 */
/**
 * @author Comp
 *
 */
public class GameServiceTest {


	/** The game. */
	Game game;

	/**
	 * Gamemode: 1=1x fighting with same action possible, 2=no fighting of action
	 * cards
	 */
	int gamemod;

	/** The p. */
	Player player;
	Player bot;

	Deck deckClosed;

	/** The open deck. */
	Deck deckOpen;

	/** The players. */
	List<Player> players;

	/** The game service. */
	GameService gameService;

	/** The deck service. */
	DeckService deckService = Mockito.mock(DeckService.class);

	/** The rule service. */
	RuleService ruleService = Mockito.mock(RuleService.class);
	
	/** The rule service. */
	VirtualPlayer virtualPlayer = Mockito.mock(VirtualPlayer.class);

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		// SetUp Game
		gameService = new GameService();
		gameService.setDeckService(deckService);
		gameService.setRuleService(ruleService);

		game = new Game();
		player = new Player("Dummy");
		bot = new Player("Bot");
		players = game.getPlayers();
		players.add(0, player);
		players.add(1, bot);
		game.setGameMode(1);

		deckClosed = game.getDeckClosed();
		deckOpen = game.getDeckOpen();

		deckClosed.getCards().add(new Card(Suit.Spades, Rank.SIX));
		deckClosed.getCards().add(new Card(Suit.Spades, Rank.SEVEN));
		deckClosed.getCards().add(new Card(Suit.Spades, Rank.EIGHT));
		deckClosed.getCards().add(new Card(Suit.Spades, Rank.NINE));
		deckClosed.getCards().add(new Card(Suit.Spades, Rank.TEN));
		deckClosed.getCards().add(new Card(Suit.Spades, Rank.Jack));
		deckClosed.getCards().add(new Card(Suit.Spades, Rank.Queen));
		deckClosed.getCards().add(new Card(Suit.Spades, Rank.King));
		deckClosed.getCards().add(new Card(Suit.Spades, Rank.ACE));

		deckClosed.getCards().add(new Card(Suit.Hearts, Rank.SIX));
		deckClosed.getCards().add(new Card(Suit.Hearts, Rank.SEVEN));
		deckClosed.getCards().add(new Card(Suit.Hearts, Rank.EIGHT));
		deckClosed.getCards().add(new Card(Suit.Hearts, Rank.NINE));
		deckClosed.getCards().add(new Card(Suit.Hearts, Rank.TEN));
		deckClosed.getCards().add(new Card(Suit.Hearts, Rank.Jack));
		deckClosed.getCards().add(new Card(Suit.Hearts, Rank.Queen));
		deckClosed.getCards().add(new Card(Suit.Hearts, Rank.King));
		deckClosed.getCards().add(new Card(Suit.Hearts, Rank.ACE));

		deckClosed.getCards().add(new Card(Suit.Diamonds, Rank.SIX));
		deckClosed.getCards().add(new Card(Suit.Diamonds, Rank.SEVEN));
		deckClosed.getCards().add(new Card(Suit.Diamonds, Rank.EIGHT));
		deckClosed.getCards().add(new Card(Suit.Diamonds, Rank.NINE));
		deckClosed.getCards().add(new Card(Suit.Diamonds, Rank.TEN));
		deckClosed.getCards().add(new Card(Suit.Diamonds, Rank.Jack));
		deckClosed.getCards().add(new Card(Suit.Diamonds, Rank.Queen));
		deckClosed.getCards().add(new Card(Suit.Diamonds, Rank.King));
		deckClosed.getCards().add(new Card(Suit.Diamonds, Rank.ACE));

		deckClosed.getCards().add(new Card(Suit.Clubs, Rank.SIX));
		deckClosed.getCards().add(new Card(Suit.Clubs, Rank.SEVEN));
		deckClosed.getCards().add(new Card(Suit.Clubs, Rank.EIGHT));
		deckClosed.getCards().add(new Card(Suit.Clubs, Rank.NINE));
		deckClosed.getCards().add(new Card(Suit.Clubs, Rank.TEN));
		deckClosed.getCards().add(new Card(Suit.Clubs, Rank.Jack));
		deckClosed.getCards().add(new Card(Suit.Clubs, Rank.Queen));
		deckClosed.getCards().add(new Card(Suit.Clubs, Rank.King));
		deckClosed.getCards().add(new Card(Suit.Clubs, Rank.ACE));

	}

	
	/**
	 * Test if game has been set up; player added, gamemode is set
	 */
	@Test
	public void testInitGame() {
		String playerName = "Dummy";
		Game game1= new Game();
		
		Mockito.when(deckService.generateDeck()).thenReturn(deckClosed);
		
		gameService.initGame(game1, playerName, 1,1);
		
		Assert.assertEquals(playerName, game1.getPlayers().get(0).getNickname());
		Assert.assertEquals("Bot 1", game1.getPlayers().get(1).getNickname());
		Assert.assertEquals(1, game1.getGameMode());
	}
	
	@Test
	public void testAddPlayer() {
		Player p = new Player("test");
		int countPlayersBefore = game.getPlayers().size();
		gameService.addPlayer(game, p);
		int countPlayersAfter = game.getPlayers().size();
		
		Assert.assertTrue(game.getPlayers().contains(p));
		Assert.assertEquals(countPlayersBefore+1, countPlayersAfter);
	}
	
	@Test
	public void testGetPlayerCards() {
		Card testCard = new Card(Suit.Clubs,Rank.ACE);
		Deck playerDeck = player.getpDeck();
		playerDeck.getCards().add(testCard);
		
		List<String> expected = new ArrayList<String>();
		expected.add(testCard.toString());
	
		Mockito.when(deckService.getCardsAsStringList(playerDeck)).thenReturn(expected);
		
		List<String> actual = gameService.getPlayerCards(game);
		
		Assert.assertEquals(expected, actual);
		Assert.assertTrue(playerDeck.getCards().contains(testCard));
	}
	/**
	 * Test if player hand is filled.
	 */
	@Test
	public void testHandout() {
		Card card= new Card(Suit.Clubs,Rank.ACE);
		Deck deck = players.get(0).getpDeck();
		
		Mockito.when(deckService.alertEmptyDeck(deckClosed)).thenReturn(false);
		Mockito.when(deckService.cardChangeDeck(deckClosed, deck)).thenReturn(card);
		Mockito.when(deckService.alertEmptyDeck(deckClosed)).thenReturn(false);
		Mockito.when(deckService.cardChangeDeck(deckClosed, deck)).thenCallRealMethod();
		gameService.handOut(game, players);
		
		Assert.assertEquals(5,players.get(0).getpDeck().getCards().size());
	}
	/**
	 * Test if player took one card.
	 */
	@Test
	public void testTakeCard() {
		Card lastCard = deckClosed.getCards().get(deckClosed.getCards().size() - 1);
		Deck deck = player.getpDeck();
		
		Mockito.when(deckService.alertEmptyDeck(deckClosed)).thenReturn(false);
		Mockito.when(deckService.cardChangeDeck(deckClosed, deck)).thenCallRealMethod();
		
		gameService.takeCard(game, player);
		
		
		Assert.assertTrue(player.getpDeck().getCards().contains(lastCard));
		Assert.assertFalse(deckClosed.getCards().contains(lastCard));
	}
	
	
	/**
	 *Tests if player takes two cards from closed deck as punishment
	 */
	@Test
	public void testPunishMau() {
		int playerNr = 0;
		Card card1 = deckClosed.getCards().get(deckClosed.getCards().size() - 1);
		Card card2 = deckClosed.getCards().get(deckClosed.getCards().size() - 2);
		Deck deck = player.getpDeck();
		
		Mockito.when(deckService.alertEmptyDeck(deckClosed)).thenReturn(false);
		Mockito.when(deckService.cardChangeDeck(deckClosed, deck)).thenCallRealMethod();
		
		gameService.punishMau(game, playerNr); 
		
		Assert.assertTrue(player.getpDeck().getCards().contains(card1));
		Assert.assertFalse(deckClosed.getCards().contains(card1));
		Assert.assertTrue(player.getpDeck().getCards().contains(card2));
		Assert.assertFalse(deckClosed.getCards().contains(card2));
	}
	/**
	 * 
	 */
	@Test
	public void testPlaceCard() {
		Card card = new Card(Suit.Clubs,Rank.ACE);
		player.getpDeck().getCards().add(card);
		boolean beforePlayer = player.getpDeck().getCards().contains(card);
		boolean beforeOpenDeck = game.getDeckOpen().getCards().contains(card);
		
		boolean actual=false;
		try {
			actual = gameService.placeCard(game, player, card);
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		Assert.assertTrue(actual);
		Assert.assertTrue(beforePlayer);
		Assert.assertFalse(beforeOpenDeck);
		Assert.assertTrue(game.getDeckOpen().getCards().contains(card));
		Assert.assertFalse(player.getpDeck().getCards().contains(card));
	}
	/**
	 * Test next player.
	 */
	@Test
	public void testNextPlayer() {
		Assert.assertEquals(game.getWhosTurn(), 0);
		gameService.nextPlayer(game);
		Assert.assertEquals(game.getWhosTurn(), 1);
	}
	/**
	 * Test getOpenCard.
	 */
	@Test
	public void testGetOpenCard() {
		Card testCard = new Card(Suit.Clubs,Rank.ACE);
		game.getDeckOpen().getCards().add(testCard);
		Card expected = game.getDeckOpen().getCards().get(game.getDeckOpen().getCards().size()-1);
		
		Mockito.when(deckService.getOpenCard(game.getDeckOpen())).thenReturn(testCard);
		Card actual = gameService.getOpenCard(game); 
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testCountBotCards() {
		bot.getpDeck().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		bot.getpDeck().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		bot.getpDeck().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		bot.getpDeck().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		
		List<Integer> actual= gameService.countBotCards(game); 
		
		Assert.assertEquals(Integer.valueOf(4), actual.get(0));
	}
	
	/**
	 * 
	 */
	/**
	 * 
	 */
	@Test
	public void testCheckCardsAndMau_winner() {
		
		Mockito.when(ruleService.checkCardsAndMau(player.getpDeck().getCards().size(), player.isMau())).thenCallRealMethod();
		String actual = ruleService.checkCardsAndMau(player.getpDeck().getCards().size(), player.isMau()); 
		
		Assert.assertEquals("winner", actual);
	}
	
	/**
	 * 
	 */
	/**
	 * 
	 */
	@Test
	public void testCheckCardsAndMau_proceed() {
		player.getpDeck().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		player.getpDeck().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		player.getpDeck().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		
		Mockito.when(ruleService.checkCardsAndMau(player.getpDeck().getCards().size(), player.isMau())).thenCallRealMethod();
		String actual = ruleService.checkCardsAndMau(player.getpDeck().getCards().size(), player.isMau()); 
		
		Assert.assertEquals("proceed", actual);
	}
	
	/**
	 * 
	 */
	@Test
	public void testCheckCardsAndMau_nomau() {
		player.getpDeck().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		
		Mockito.when(ruleService.checkCardsAndMau(player.getpDeck().getCards().size(), player.isMau())).thenCallRealMethod();
		String actual = ruleService.checkCardsAndMau(player.getpDeck().getCards().size(), player.isMau()); 
		
		Assert.assertEquals("nomau", actual);
	}
	
	/**
	 * 
	 */
	@Test
	public void testCheckCardsAndMau_evalmau() {
		player.getpDeck().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		player.getpDeck().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		player.getpDeck().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		player.setMau(true);
		
		Mockito.when(ruleService.checkCardsAndMau(player.getpDeck().getCards().size(), player.isMau())).thenCallRealMethod();
		String actual = ruleService.checkCardsAndMau(player.getpDeck().getCards().size(), player.isMau()); 
		
		Assert.assertEquals("evalmau", actual);
	}
	
	/**
	 * 
	 */
	@Test
	public void testCheckCardsAndMau_mau() {
		player.getpDeck().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		player.setMau(true);
		Mockito.when(ruleService.checkCardsAndMau(player.getpDeck().getCards().size(), player.isMau())).thenCallRealMethod();
		String actual = ruleService.checkCardsAndMau(player.getpDeck().getCards().size(), player.isMau()); 
		
		Assert.assertEquals("mau", actual);
	}
	
	/**
	 * 
	 */
	@Test
	public void testSetPlayersWishFalse() {
		player.setWish(true);
		bot.setWish(true);
		
		gameService.setPlayersWishFalse(game);
		
		Assert.assertFalse(player.isWish());
		Assert.assertFalse(bot.isWish());
	}
	
	/**
	 * 
	 */
	@Test
	public void testUnsetMau() {
		Card card = new Card(Suit.Clubs,Rank.ACE);
		player.getpDeck().getCards().add(card);
		player.getpDeck().getCards().add(card);
		player.setMau(true);
		bot.getpDeck().getCards().add(card);
		bot.setMau(true);
		
		gameService.unsetMau(game); 
		
		Assert.assertFalse(player.isMau());
		Assert.assertTrue(bot.isMau());
		
	}
	
	
	@Test
	public void testSetCurrentWishPlayer_Hearts() {
		int currentWish=1;
		player.setWish(true);
		gameService.setCurrentWishPlayer(game, currentWish); 
		
		Assert.assertEquals(game.getCurrentWish(),Suit.Hearts.toString());
		Assert.assertFalse(player.isWish());
	}
	@Test
	public void testSetCurrentWishPlayer_Diamonds() {
		int currentWish=2;
		player.setWish(true);
		gameService.setCurrentWishPlayer(game, currentWish); 
		
		Assert.assertEquals(game.getCurrentWish(),Suit.Diamonds.toString());
		Assert.assertFalse(player.isWish());
	}
	
	/**
	 * 
	 */
	@Test
	public void testSetCurrentWishPlayer_Clubs() {
		int currentWish=3;
		player.setWish(true);
		gameService.setCurrentWishPlayer(game, currentWish); 
		
		Assert.assertEquals(game.getCurrentWish(),Suit.Clubs.toString());
		Assert.assertFalse(player.isWish());
	}
	
	/**
	 * 
	 */
	/**
	 * 
	 */
	@Test
	public void testSetCurrentWishPlayer_Spades() {
		int currentWish=4;
		player.setWish(true);
		gameService.setCurrentWishPlayer(game, currentWish); 
		
		Assert.assertEquals(game.getCurrentWish(),Suit.Spades.toString());
		Assert.assertFalse(player.isWish());
	}
	
	/**
	 * 
	 */
	@Test
	public void testCountScoreGame() {
		Deck deckPlayer = player.getpDeck();
		Deck deckBot = bot.getpDeck();
		deckPlayer.getCards().add(new Card(Suit.Clubs,Rank.ACE));
		deckPlayer.getCards().add(new Card(Suit.Spades,Rank.SIX));
		deckBot.getCards().add(new Card(Suit.Clubs,Rank.ACE));
		deckBot.getCards().add(new Card(Suit.Spades,Rank.SIX));
		
		Mockito.when(deckService.countScore(deckPlayer)).thenReturn(19);
		Mockito.when(deckService.countScore(deckBot)).thenReturn(19);
		
		Map<String, Integer> actual = gameService.countScoreGame(game);
		
		Assert.assertEquals(Integer.valueOf(19),actual.get("Bot"));
		Assert.assertEquals(Integer.valueOf(19),actual.get("Dummy"));
	}
	/**
	 * Test place card.
	 * @throws InvalidMoveException 
	 */
	@Test(expected=InvalidMoveException.class)
	public void testPlayerMove_CARD_INVALID_MOVE() throws InvalidMoveException {
		game.getDeckOpen().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		game.getPlayers().get(game.getWhosTurn()).getpDeck().getCards().add(new Card(Suit.Clubs,Rank.SIX));
		
		Card openCard = deckOpen.getCards().get(0);
		Card playerCard = player.getpDeck().getCards().get(0);
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("card", 1);
		
		Mockito.when(ruleService.validateCard(currentCondition, gameMode,
				currentWish, openCard, playerCard)).thenReturn(Condition.INVALID_MOVE);
		
		Mockito.when(deckService.getOpenCard(game.getDeckOpen())).thenReturn(openCard);
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (CardNotPlacedException e) {
			e.printStackTrace();
		}
		
		Assert.assertFalse(ok);
		Assert.assertTrue(player.getpDeck().getCards().contains(playerCard));
		Assert.assertFalse(deckOpen.getCards().contains(playerCard));	
	}
	/**
	 * Test place card. 
	 */
	@Test
	public void testPlayerMove_CARD_NO_EFFECT() {
		deckOpen.getCards().add(new Card(Suit.Clubs,Rank.ACE));
		player.getpDeck().getCards().add(new Card(Suit.Clubs,Rank.SIX));
		player.getpDeck().getCards().add(new Card(Suit.Hearts,Rank.SIX));
		game.setGameMode(1);
		
		Card openCard = deckOpen.getCards().get(0);
		Card playerCard = player.getpDeck().getCards().get(0);
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		boolean mauBefore = player.isMau();
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("card", 1);
		playerChoice.put("mau", 1);
		
		Mockito.when(ruleService.validateCard(currentCondition, gameMode,
				currentWish, openCard, playerCard)).thenReturn(Condition.NO_EFFECT);
		
		Mockito.when(deckService.getOpenCard(game.getDeckOpen())).thenReturn(openCard);
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Assert.assertTrue(ok);
		Assert.assertFalse(player.getpDeck().getCards().contains(playerCard));
		Assert.assertTrue(deckOpen.getCards().contains(playerCard));
		Assert.assertFalse(mauBefore);
		Assert.assertTrue(player.isMau());
		
	}
	/**
	 * Test place card.
	 * 
	 */
	@Test
	public void testPlayerMove_CARD_PLUS_TWO() {
		game.getDeckOpen().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		game.getPlayers().get(game.getWhosTurn()).getpDeck().getCards().add(new Card(Suit.Clubs,Rank.SIX));
		
		Card openCard = deckOpen.getCards().get(0);
		Card playerCard = player.getpDeck().getCards().get(0);
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("card", 1);
		
		Mockito.when(ruleService.validateCard(currentCondition, gameMode,
				currentWish, openCard, playerCard)).thenReturn(Condition.PLUS_TWO);
		
		Mockito.when(deckService.getOpenCard(game.getDeckOpen())).thenReturn(openCard);
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Assert.assertTrue(ok);
		Assert.assertFalse(player.getpDeck().getCards().contains(playerCard));
		Assert.assertTrue(deckOpen.getCards().contains(playerCard));
		
	}
	/**
	 * Test place card.
	 */
	@Test
	public void testPlayerMove_CARD_PLUS_TWO_STRICT() {
		game.getDeckOpen().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		game.getPlayers().get(game.getWhosTurn()).getpDeck().getCards().add(new Card(Suit.Clubs,Rank.SIX));
		game.setGameMode(1);
		
		Card openCard = deckOpen.getCards().get(0);
		Card playerCard = player.getpDeck().getCards().get(0);
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("card", 1);
		
		Mockito.when(ruleService.validateCard(currentCondition, gameMode,
				currentWish, openCard, playerCard)).thenReturn(Condition.PLUS_TWO_STRICT);
		
		Mockito.when(deckService.getOpenCard(game.getDeckOpen())).thenReturn(openCard);
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Assert.assertTrue(ok);
		Assert.assertFalse(player.getpDeck().getCards().contains(playerCard));
		Assert.assertTrue(deckOpen.getCards().contains(playerCard));
		
	}
	/**
	 * Test place card.
	 */
	@Test
	public void testPlayerMove_CARD_PLUS_FOUR() {
		game.getDeckOpen().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		game.getPlayers().get(game.getWhosTurn()).getpDeck().getCards().add(new Card(Suit.Clubs,Rank.SIX));
		
		Card openCard = deckOpen.getCards().get(0);
		Card playerCard = player.getpDeck().getCards().get(0);
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("card", 1);
		
		Mockito.when(ruleService.validateCard(currentCondition, gameMode,
				currentWish, openCard, playerCard)).thenReturn(Condition.PLUS_FOUR);
		
		Mockito.when(deckService.getOpenCard(game.getDeckOpen())).thenReturn(openCard);
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Assert.assertTrue(ok);
		Assert.assertFalse(player.getpDeck().getCards().contains(playerCard));
		Assert.assertTrue(deckOpen.getCards().contains(playerCard));
		
	}
	/**
	 * Test place card.
	 */
	@Test
	public void testPlayerMove_CARD_SKIP() {
		game.getDeckOpen().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		game.getPlayers().get(game.getWhosTurn()).getpDeck().getCards().add(new Card(Suit.Clubs,Rank.SIX));
		
		Card openCard = deckOpen.getCards().get(0);
		Card playerCard = player.getpDeck().getCards().get(0);
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("card", 1);
		
		Mockito.when(ruleService.validateCard(currentCondition, gameMode,
				currentWish, openCard, playerCard)).thenReturn(Condition.SKIP_SKIP);
		
		Mockito.when(deckService.getOpenCard(game.getDeckOpen())).thenReturn(openCard);
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Assert.assertTrue(ok);
		Assert.assertFalse(player.getpDeck().getCards().contains(playerCard));
		Assert.assertTrue(deckOpen.getCards().contains(playerCard));
		
	}
	/**
	 * Test place card.
	 */
	@Test
	public void testPlayerMove_CARD_SKIP_SKIP() {
		game.getDeckOpen().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		game.getPlayers().get(game.getWhosTurn()).getpDeck().getCards().add(new Card(Suit.Clubs,Rank.SIX));
		game.setGameMode(1);
		
		Card openCard = deckOpen.getCards().get(0);
		Card playerCard = player.getpDeck().getCards().get(0);
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("card", 1);
		
		Mockito.when(ruleService.validateCard(currentCondition, gameMode,
				currentWish, openCard, playerCard)).thenReturn(Condition.SKIP_SKIP);
		
		Mockito.when(deckService.getOpenCard(game.getDeckOpen())).thenReturn(openCard);
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Assert.assertTrue(ok);
		Assert.assertFalse(player.getpDeck().getCards().contains(playerCard));
		Assert.assertTrue(deckOpen.getCards().contains(playerCard));
		
	}
	/**
	 * Test place card.
	 */
	@Test
	public void testPlayerMove_CARD_WISH() {
		game.getDeckOpen().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		game.getPlayers().get(game.getWhosTurn()).getpDeck().getCards().add(new Card(Suit.Clubs,Rank.SIX));
		
		Card openCard = deckOpen.getCards().get(0);
		Card playerCard = player.getpDeck().getCards().get(0);
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("card", 1);
		
		Mockito.when(ruleService.validateCard(currentCondition, gameMode,
				currentWish, openCard, playerCard)).thenReturn(Condition.WISH);
		
		Mockito.when(deckService.getOpenCard(game.getDeckOpen())).thenReturn(openCard);
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Assert.assertTrue(ok);
		Assert.assertFalse(player.getpDeck().getCards().contains(playerCard));
		Assert.assertTrue(deckOpen.getCards().contains(playerCard));
		
	}
	/**
	 * Test place card.
	 */
	@Test
	public void testPlayerMove_CARD_WISH_WISH() {
		game.getDeckOpen().getCards().add(new Card(Suit.Clubs,Rank.ACE));
		game.getPlayers().get(game.getWhosTurn()).getpDeck().getCards().add(new Card(Suit.Clubs,Rank.SIX));
		game.setGameMode(1);
		
		Card openCard = deckOpen.getCards().get(0);
		Card playerCard = player.getpDeck().getCards().get(0);
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("card", 1);
		
		Mockito.when(ruleService.validateCard(currentCondition, gameMode,
				currentWish, openCard, playerCard)).thenReturn(Condition.WISH_WISH);
		
		Mockito.when(deckService.getOpenCard(game.getDeckOpen())).thenReturn(openCard);
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			e.printStackTrace();
		}
		
		Assert.assertTrue(ok);
		Assert.assertFalse(player.getpDeck().getCards().contains(playerCard));
		Assert.assertTrue(deckOpen.getCards().contains(playerCard));
		
	}

	/**
	 * Test place card.
	 * @throws InvalidMoveException 
	 */
	@Test(expected = InvalidMoveException.class)
	public void testPlayerMove_Action_INVALID_MOVE() throws InvalidMoveException {
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		int cardsCountBefore = player.getpDeck().getCards().size();
		String conditionBefore = game.getCondition().toString();
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		
		Mockito.when(ruleService.validateAction(currentCondition, gameMode,
				currentWish, playerChoice)).thenReturn(Condition.INVALID_MOVE);
		Mockito.when(deckService.alertEmptyDeck(deckClosed)).thenReturn(false);
		Mockito.when(deckService.cardChangeDeck(deckClosed, player.getpDeck())).thenCallRealMethod();
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (CardNotPlacedException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Test place card.
	 */
	@Test
	public void testPlayerMove_Action_NO_EFFECT() {
		game.setCondition(Condition.NO_EFFECT);
		
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		int cardsCountBefore = player.getpDeck().getCards().size();
		Condition condBefore = game.getCondition();
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("take_one", 1);
		
		Mockito.when(ruleService.validateAction(currentCondition, gameMode,
				currentWish, playerChoice)).thenReturn(Condition.NO_EFFECT);
		Mockito.when(ruleService.howMuchToTake(condBefore,Condition.NO_EFFECT)).thenCallRealMethod();
		Mockito.when(deckService.alertEmptyDeck(deckClosed)).thenReturn(false);
		Mockito.when(deckService.cardChangeDeck(deckClosed, player.getpDeck())).thenCallRealMethod();
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int cardsCountAfter = player.getpDeck().getCards().size();
		Condition condAfter = game.getCondition();
		
		Assert.assertTrue(ok);
		Assert.assertEquals(cardsCountBefore+1,cardsCountAfter);
		Assert.assertEquals(condBefore, Condition.NO_EFFECT);
		Assert.assertEquals(condAfter, Condition.NO_EFFECT);	
	}
	
	/**
	 * Test place card.
	 */
	@Test
	public void testPlayerMove_Action_SKIP() {
		game.setCondition(Condition.SKIP);
		
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		int cardsCountBefore = player.getpDeck().getCards().size();
		Condition conditionBefore = currentCondition;
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("skip", 1);
		
		Mockito.when(ruleService.validateAction(currentCondition, gameMode,
				currentWish, playerChoice)).thenReturn(Condition.NO_EFFECT);
		Mockito.when(ruleService.howMuchToTake(conditionBefore,Condition.NO_EFFECT)).thenCallRealMethod();
		Mockito.when(deckService.alertEmptyDeck(deckClosed)).thenReturn(false);
		Mockito.when(deckService.cardChangeDeck(deckClosed, player.getpDeck())).thenCallRealMethod();
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int cardsCountAfter = player.getpDeck().getCards().size();
		Condition conditionAfter = game.getCondition();
		
		Assert.assertTrue(ok);
		Assert.assertEquals(cardsCountBefore,cardsCountAfter);
		Assert.assertEquals(conditionBefore, Condition.SKIP);
		Assert.assertEquals(conditionAfter, Condition.NO_EFFECT);
		
	}
	
	/**
	 * Test place card.
	 */
	@Test
	public void testPlayerMove_Action_SKIP_SKIP() {
		game.setCondition(Condition.SKIP_SKIP);
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		int cardsCountBefore = player.getpDeck().getCards().size();
		String conditionBefore = game.getCondition().toString();
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("skip_skip", 1);
		
		Mockito.when(ruleService.validateAction(currentCondition, gameMode,
				currentWish, playerChoice)).thenReturn(Condition.NO_EFFECT);
		Mockito.when(deckService.alertEmptyDeck(deckClosed)).thenReturn(false);
		Mockito.when(deckService.cardChangeDeck(deckClosed, player.getpDeck())).thenCallRealMethod();
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int cardsCountAfter = player.getpDeck().getCards().size();
		String conditionAfter = game.getCondition().toString();
		
		Assert.assertTrue(ok);
		Assert.assertEquals(cardsCountBefore, cardsCountAfter);
		Assert.assertEquals(conditionBefore,Condition.SKIP_SKIP.toString());
		Assert.assertEquals(conditionAfter, Condition.NO_EFFECT.toString());
	}
	
	/**
	 * Test place card.
	 */
	@Test
	public void testPlayerMove_Action_PLUS_TWO() {
		game.setCondition(Condition.PLUS_TWO);
		
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		int cardsCountBefore = player.getpDeck().getCards().size();
		Condition conditionBefore = game.getCondition();
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("take_two", 1);
		
		Mockito.when(ruleService.validateAction(conditionBefore, gameMode,
				currentWish, playerChoice)).thenCallRealMethod();
		Mockito.when(ruleService.howMuchToTake(conditionBefore,Condition.NO_EFFECT)).thenCallRealMethod();
		Mockito.when(deckService.alertEmptyDeck(deckClosed)).thenReturn(false);
		Mockito.when(deckService.cardChangeDeck(deckClosed, player.getpDeck())).thenCallRealMethod();
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int cardsCountAfter = player.getpDeck().getCards().size();
		Condition conditionAfter = game.getCondition();
		
		Assert.assertTrue(ok);
		Assert.assertEquals(cardsCountBefore+2,cardsCountAfter);
		Assert.assertEquals(conditionBefore, Condition.PLUS_TWO);
		Assert.assertEquals(conditionAfter, Condition.NO_EFFECT);	
	}
	
	/**
	 * Test place card.
	 */
	@Test
	public void testPlayerMove_Action_PLUS_TWO_STRICT() {
		game.setCondition(Condition.PLUS_TWO_STRICT);
		
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		int cardsCountBefore = player.getpDeck().getCards().size();
		Condition conditionBefore = currentCondition;
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("take_two", 1);
		
		Mockito.when(ruleService.validateAction(currentCondition, gameMode,
				currentWish, playerChoice)).thenReturn(Condition.NO_EFFECT);
		Mockito.when(ruleService.howMuchToTake(conditionBefore,Condition.NO_EFFECT)).thenCallRealMethod();

		Mockito.when(deckService.alertEmptyDeck(deckClosed)).thenReturn(false);
		Mockito.when(deckService.cardChangeDeck(deckClosed, player.getpDeck())).thenCallRealMethod();
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int cardsCountAfter = player.getpDeck().getCards().size();
		Condition conditionAfter = game.getCondition();
		
		Assert.assertTrue(ok);
		Assert.assertEquals(cardsCountBefore+2,cardsCountAfter);
		Assert.assertEquals(conditionBefore, Condition.PLUS_TWO_STRICT);
		Assert.assertEquals(conditionAfter, Condition.NO_EFFECT);
		
	}

	/**
	 * Test place card.
	 */
	@Test
	public void testPlayerMove_Action_PLUS_FOUR() {
		game.setCondition(Condition.PLUS_FOUR);
		
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		int cardsCountBefore = player.getpDeck().getCards().size();
		Condition conditionBefore = currentCondition;
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("take_four", 1);
		
		Mockito.when(ruleService.validateAction(currentCondition, gameMode,
				currentWish, playerChoice)).thenReturn(Condition.NO_EFFECT);
		Mockito.when(ruleService.howMuchToTake(conditionBefore,Condition.NO_EFFECT)).thenCallRealMethod();

		Mockito.when(deckService.alertEmptyDeck(deckClosed)).thenReturn(false);
		Mockito.when(deckService.cardChangeDeck(deckClosed, player.getpDeck())).thenCallRealMethod();
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int cardsCountAfter = player.getpDeck().getCards().size();
		Condition conditionAfter = game.getCondition();
		
		Assert.assertTrue(ok);
		Assert.assertEquals(cardsCountBefore+4,cardsCountAfter);
		Assert.assertEquals(conditionBefore, Condition.PLUS_FOUR);
		Assert.assertEquals(conditionAfter, Condition.NO_EFFECT);
		
	}

	/**
	 * Test place card.
	 */
	@Test
	public void testPlayerMove_Action_WISH() {
		game.setCondition(Condition.WISH);
		
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		int cardsCountBefore = player.getpDeck().getCards().size();
		Condition conditionBefore = currentCondition;
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("take_one", 1);
		
		Mockito.when(ruleService.validateAction(currentCondition, gameMode,
				currentWish, playerChoice)).thenReturn(Condition.WISH_WISH);
		Mockito.when(ruleService.howMuchToTake(conditionBefore,Condition.WISH_WISH)).thenCallRealMethod();

		Mockito.when(deckService.alertEmptyDeck(deckClosed)).thenReturn(false);
		Mockito.when(deckService.cardChangeDeck(deckClosed, player.getpDeck())).thenCallRealMethod();
		
		boolean ok = false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int cardsCountAfter = player.getpDeck().getCards().size();
		Condition conditionAfter = game.getCondition();
		
		Assert.assertTrue(ok);
		Assert.assertEquals(cardsCountBefore+1,cardsCountAfter);
		Assert.assertEquals(conditionBefore, Condition.WISH);
		Assert.assertEquals(conditionAfter, Condition.WISH_WISH);
		
	}
	
	/**
	 * Test place card.
	 */
	@Test
	public void testPlayerMove_Action_WISH_WISH() {
		game.setCondition(Condition.WISH_WISH);
		
		Condition currentCondition = game.getCondition();
		int gameMode = game.getGameMode();
		String currentWish = game.getCurrentWish();
		
		int cardsCountBefore = player.getpDeck().getCards().size();
		Condition conditionBefore = currentCondition;
		
		Map<String, Integer> playerChoice = new HashMap<String,Integer>();
		playerChoice.put("take_one", 1);
		
		Mockito.when(ruleService.validateAction(currentCondition, gameMode,
				currentWish, playerChoice)).thenReturn(Condition.WISH_WISH);
		Mockito.when(ruleService.howMuchToTake(conditionBefore,Condition.WISH_WISH)).thenCallRealMethod();

		Mockito.when(deckService.alertEmptyDeck(deckClosed)).thenReturn(false);
		Mockito.when(deckService.cardChangeDeck(deckClosed, player.getpDeck())).thenCallRealMethod();
		
		boolean ok=false;
		try {
			ok = gameService.playerMove(game,playerChoice);
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardNotPlacedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int cardsCountAfter = player.getpDeck().getCards().size();
		Condition conditionAfter = game.getCondition();
		
		Assert.assertTrue(ok);
		Assert.assertEquals(cardsCountBefore+1,cardsCountAfter);
		Assert.assertEquals(conditionBefore, Condition.WISH_WISH);
		Assert.assertEquals(conditionAfter, Condition.WISH_WISH);
		
	}
	
	
}
